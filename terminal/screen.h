#ifndef TERMINAL_SCREEN_H
#define TERMINAL_SCREEN_H

#include <stdint.h>
#include <string>
#include <terminal/screenattribute.h>

namespace terminal {

class Screen
{
public:
    Screen();
    virtual ~Screen();

    unsigned int width() const { return tsm_screen_get_width(_con); }
    unsigned int height() const { return tsm_screen_get_height(_con); }

    int resize(unsigned int x, unsigned int y) { return tsm_screen_resize(_con,x,y); }
    int setMargins(unsigned int top, unsigned int bottom) { return tsm_screen_set_margins(_con, top, bottom); }

    void setMaxScrollBack(unsigned int max) { tsm_screen_set_max_sb(_con, max); }
    unsigned int maxScrollBack() const { return tsm_screen_get_max_sb(_con); }
    void clearScrollBack() { tsm_screen_clear_sb(_con); }
    unsigned int scrollBackCount() const { return tsm_screen_get_sb_count(_con); }

    int scrollBackPosition() const { return tsm_screen_get_sb_pos(_con); }

    void scrollBackUp(unsigned int num) { tsm_screen_sb_up(_con, num); }
    void scrollBackDown(unsigned int num) { tsm_screen_sb_down(_con, num); }
    void scrollBackPageUp(unsigned int num) { tsm_screen_sb_page_up(_con, num); }
    void scrollBackPageDown(unsigned int num) { tsm_screen_sb_page_down(_con, num); }
    void scrollBackReset() { tsm_screen_sb_reset(_con); }

    void setDefaultAttribute(const ScreenAttribute &attr) { tsm_screen_set_def_attr(_con, &attr); }
    void reset() { tsm_screen_reset(_con); }
    void setFlags(unsigned int flags) { tsm_screen_set_flags(_con, flags); }
    void resetFlags(unsigned int flags) { tsm_screen_reset_flags(_con, flags); }
    unsigned int flags() const { return tsm_screen_get_flags(_con); }

    unsigned int cursorX() const { return tsm_screen_get_cursor_x(_con); }
    unsigned int cursorY() const { return tsm_screen_get_cursor_y(_con); }

    void setTabstop() { tsm_screen_set_tabstop(_con); }
    void resetTabstop() { tsm_screen_reset_tabstop(_con); }
    void resetAllTabstops() { tsm_screen_reset_all_tabstops(_con); }

    void write(tsm_symbol_t ch, const ScreenAttribute &attr) { tsm_screen_write(_con, ch, &attr); }
    void newLine() { tsm_screen_newline(_con); }

    void scrollUp(unsigned int num) { tsm_screen_scroll_up(_con, num); }
    void scrollDown(unsigned int num) { tsm_screen_scroll_down(_con, num); }
    void moveTo(unsigned int x, unsigned int y) { tsm_screen_move_to(_con, x, y); }
    void moveUp(unsigned int num, bool scroll) { tsm_screen_move_up(_con, num, scroll); }
    void moveDown(unsigned int num, bool scroll) { tsm_screen_move_down(_con, num, scroll); }
    void moveLeft(unsigned int num) { tsm_screen_move_left(_con, num); }
    void moveRight(unsigned int num) { tsm_screen_move_right(_con, num); }
    void moveLineEnd() { tsm_screen_move_line_end(_con); }
    void moveLineHome() { tsm_screen_move_line_home(_con); }
    void tabRight(unsigned int num) { tsm_screen_tab_right(_con, num); }
    void tabLeft(unsigned int num) { tsm_screen_move_left(_con, num); }
    void insertLines(unsigned int num) { tsm_screen_insert_lines(_con, num); }
    void deleteLines(unsigned int num) { tsm_screen_delete_lines(_con, num); }
    void insertChars(unsigned int num) { tsm_screen_insert_chars(_con, num); }
    void deleteChars(unsigned int num) { tsm_screen_delete_chars(_con, num); }
    void eraseCursor() { tsm_screen_erase_cursor(_con); }
    void eraseChars(unsigned int num) { tsm_screen_erase_chars(_con, num); }
    void eraseCursorToEnd(bool protect) { tsm_screen_erase_cursor_to_end(_con, protect); }
    void eraseHomeToCursor(bool protect) { tsm_screen_erase_home_to_cursor(_con, protect); }
    void eraseCurrentLine(bool protect) { tsm_screen_erase_current_line(_con, protect); }
    void eraseScreenToCursor(bool protect) { tsm_screen_erase_screen_to_cursor(_con, protect); }
    void eraseCursorToScreen(bool protect) { tsm_screen_erase_cursor_to_screen(_con, protect); }
    void eraseScreen(bool protect) { tsm_screen_erase_screen(_con, protect); }

    void selectionReset() { tsm_screen_selection_reset(_con); }
    void selectionStart(unsigned int posx, unsigned int posy) { tsm_screen_selection_start(_con, posx, posy); }
    void selectionTarget(unsigned int posx, unsigned int posy) { tsm_screen_selection_target(_con, posx, posy); }
    std::string selectionCopy();

    tsm_age_t draw();


    struct tsm_screen *getInternal();
protected:
    virtual int onDraw(uint32_t id, const uint32_t *ch, size_t len,
               unsigned int width, unsigned int posx,
               unsigned int posy, const ScreenAttribute &attr,
               tsm_age_t age);

private:
    static int onDrawStatic(struct tsm_screen *con, uint32_t id, const uint32_t *ch, size_t len, unsigned int width,
                            unsigned int posx, unsigned int posy, const ScreenAttribute *attr, tsm_age_t age, void *data);
    struct tsm_screen *_con;
};

}

#endif // TERMINAL_SCREEN_H
