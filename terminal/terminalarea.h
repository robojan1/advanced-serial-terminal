#ifndef TERMINAL_TERMINALAREA_H
#define TERMINAL_TERMINALAREA_H

#include <QWidget>
#include <terminal/screen.h>
#include <terminal/vte.h>
#include <memory>

class QMenu;

namespace terminal {

class TerminalArea: public QWidget, public Screen, public VTE
{
    Q_OBJECT
public:
    TerminalArea(QWidget *parent = nullptr);
    virtual ~TerminalArea();

    QSize sizeHint() const override;
    QSize terminalSize() const;
    const QString &title() const;

    QVariant inputMethodQuery(Qt::InputMethodQuery) const override;

signals:
    void inputReceived(const QByteArray &data);
    void terminalResized(QSize size);
    void userInputReceived();
    void titleChanged();

public slots:
    void writeOutput(const QByteArray &data);

protected:
    int onDraw(uint32_t id, const uint32_t *ch, size_t len,
               unsigned int width, unsigned int posx, unsigned int posy,
               const ScreenAttribute &attr, tsm_age_t age) override;
    void onWrite(const char *u8, size_t len) override;
    void onOSC(int cmd, size_t len, const uint32_t *arg) override;
    void paintEvent(QPaintEvent *event) override;
    void resizeEvent(QResizeEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;
    void inputMethodEvent(QInputMethodEvent *) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseDoubleClickEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;

private:
    std::shared_ptr<QPainter> _activePainter;
    tsm_age_t _age;
    QSize _blockSize;
    QRegion _activeRegion;
    QPoint _lastSelectedPos;
    QPoint _mouseStartPoint;
    bool _hasSelection;
    QMenu *_rightClickMenu;
    QAction *_rightClickCopyAction;
    QAction *_rightClickPasteAction;
    QString _title;

    void calculateBlockSize();
    void showRightClickMenu(QPoint pos);
    void createRightClickMenu();
    void copySelection();
    void inputFromClipboard();
    bool hasValidClipboardData();
    void copyToSelectionBuffer();
    void inputFromSelectionBuffer();

};

}


#endif // TERMINAL_TERMINAL_H
