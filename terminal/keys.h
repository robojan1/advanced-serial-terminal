#ifndef TERMINAL_KEYS_H
#define TERMINAL_KEYS_H

#include <libtsm.h>
#include <Qt>

uint32_t keyToKeySym(Qt::Key key);
tsm_vte_modifier keyModtoVteMod(Qt::KeyboardModifiers mod);

#endif // TERMINAL_KEYS_H
