#include <terminal/screen.h>
#include <terminal/log.h>

#include <stdexcept>
#include <assert.h>

terminal::Screen::Screen() : _con(nullptr)
{
    int err;
    if((err = tsm_screen_new(&_con, &terminal::log, nullptr)) < 0) {
        switch(err) {
        case -ENOMEM:
            throw std::bad_alloc();
        case -EINVAL:
            assert(err == 0);
            break;
        default:
            throw std::runtime_error("Unknown error");
        }
    }
}

terminal::Screen::~Screen()
{
    tsm_screen_unref(_con);
}

std::string terminal::Screen::selectionCopy()
{
    char *selection;
    int len = tsm_screen_selection_copy(_con, &selection);
    if(len < 0) {
        switch(len) {
        case -ENOMEM:
            throw std::bad_alloc();
        case -EINVAL:
            assert(len == 0);
            break;
        case -ENOENT:
            return "";
        default:
            throw std::runtime_error("Unknown error");
        }
    }
    std::string str(selection, static_cast<size_t>(len));
    free(selection);
    return str;
}

tsm_age_t terminal::Screen::draw()
{
    return tsm_screen_draw(_con, &terminal::Screen::onDrawStatic, this);
}

tsm_screen *terminal::Screen::getInternal()
{
    return _con;
}

int terminal::Screen::onDraw(uint32_t id, const uint32_t *ch, size_t len, unsigned int width, unsigned int posx, unsigned int posy, const terminal::ScreenAttribute &attr, tsm_age_t age)
{
    (void)id;
    (void)ch;
    (void)len;
    (void)width;
    (void)posx;
    (void)posy;
    (void)attr;
    (void)age;
    return 0;
}

int terminal::Screen::onDrawStatic(tsm_screen *con, uint32_t id, const uint32_t *ch,
                                   size_t len, unsigned int width, unsigned int posx,
                                   unsigned int posy, const terminal::ScreenAttribute *attr,
                                   tsm_age_t age, void *data)
{
    (void)con;
    terminal::Screen *self = reinterpret_cast<terminal::Screen *>(data);
    return self->onDraw(id, ch, len, width, posx, posy, *attr, age);
}
