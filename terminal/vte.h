#ifndef TERMINAL_VTE_H
#define TERMINAL_VTE_H

#include <stdint.h>
#include <libtsm.h>
#include <terminal/screen.h>
#include <terminal/screenattribute.h>

namespace terminal {

class VTE
{
public:
    VTE(Screen *screen);
    virtual ~VTE();

    int setPalette(const char *palette) { return tsm_vte_set_palette(_vte, palette); }
    int setPalette(const std::string &palette) { return setPalette(palette.c_str()); }

    void getDefaultAttribute(ScreenAttribute &out) { tsm_vte_get_def_attr(_vte, &out); }

    void reset() { tsm_vte_reset(_vte); }
    void hardReset() { tsm_vte_hard_reset(_vte); }
    void input(const char *u8, size_t len) { tsm_vte_input(_vte, u8, len); }
    bool handleKeyboard(uint32_t keysym, uint32_t ascii, unsigned int mods, uint32_t unicode) { return tsm_vte_handle_keyboard(_vte, keysym, ascii, mods, unicode); }

protected:
    virtual void onWrite(const char *u8, size_t len);
    virtual void onOSC(int cmd, size_t len, const uint32_t *arg);

private:
    struct tsm_vte *_vte;

    static void onWriteStatic(struct tsm_vte *vte, const char *u8, size_t len, void *data);
    static void onOSCStatic(struct tsm_vte *vte, int cmd, size_t len, const uint32_t *arg, void *data);
};

}

#endif // TERMINAL_VTE_H
