#include "log.h"
#include <QDebug>

__attribute__((__format__ (__printf__, 7, 0)))
void terminal::log(void *data, const char *file, int line, const char *func,
                   const char *subs, unsigned int sev, const char *format,
                   va_list args)
{
    (void)data;
    va_list args2;
    va_copy(args2, args);
    std::vector<char> errorMsg(64);
    int errMsgLen = vsnprintf(errorMsg.data(), errorMsg.size(), format, args);
    if(errMsgLen >= static_cast<int>(errorMsg.size())) {
        errorMsg.resize(static_cast<size_t>(errMsgLen + 1));
        vsnprintf(errorMsg.data(), errorMsg.size(), format, args2);
    }
    const char *subsystem = subs == nullptr ? "general" : subs;
    std::vector<char> buffer(static_cast<size_t>(64 + errMsgLen));
    const char * const logFormat = "[tsm-%s]%s(%s:%d) %s";
    int msgLen = snprintf(buffer.data(), buffer.size(), logFormat, subsystem, file, func, line, errorMsg.data());
    if(msgLen >= static_cast<int>(buffer.size())) {
        buffer.resize(static_cast<size_t>(msgLen + 1));
        snprintf(buffer.data(), buffer.size(), logFormat, subsystem, file, func, line, errorMsg.data());
    }

    switch(sev)
    {
    case 0:
        qFatal("%s", buffer.data());
        // Never will get here
    case 1:
    case 2:
    case 3:
        qCritical("%s", buffer.data());
        break;
    case 4:
        qWarning("%s", buffer.data());
        break;
    case 5:
    case 6:
        qInfo("%s", buffer.data());
        break;
    case 7:
        qDebug("%s", buffer.data());
    }
}
