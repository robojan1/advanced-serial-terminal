
#include <terminal/terminal.h>
#include <terminal/terminalarea.h>
#include <QScrollBar>
#include <QHBoxLayout>
#include <QDebug>
#include <QWheelEvent>

using namespace terminal;

Terminal::Terminal(QWidget *parent) :
    QWidget(parent), _term(nullptr), _scrollBar(nullptr)
{
    createInstance();
}

void Terminal::setMaxScrollBack(int max)
{
    _term->setMaxScrollBack(static_cast<unsigned int>(max));
}

int Terminal::maxScrollBack() const
{
    return static_cast<int>(_term->maxScrollBack());
}

const QString &Terminal::title() const
{
    return _term->title();
}

void Terminal::writeOutput(const QByteArray &data)
{
    _term->writeOutput(data);

    auto sbCount = static_cast<int>(_term->scrollBackCount());
    auto sbPos = static_cast<int>(_term->scrollBackPosition());
    auto scrollBarValue = sbCount - sbPos;
    _scrollBar->setRange(0, sbCount);
    if(sbPos == 0 && _scrollBar->value() != scrollBarValue) {
        _scrollBar->setValue(scrollBarValue);
    }
}

void Terminal::onScrollbarValueChanged(int value)
{
    value = _scrollBar->maximum() - value;
    auto sbPos = _term->scrollBackPosition();
    auto diff = value - sbPos;
    if(diff != 0) {
        if(diff > 0) {
            _term->scrollBackUp(diff);
        } else {
            diff = -diff;
            _term->scrollBackDown(diff);
        }
        _term->update();
    }
}

void Terminal::onUserInput()
{
    _term->scrollBackReset();
    _term->update();
}

void Terminal::createInstance()
{
    _term = new TerminalArea(this);
    _scrollBar = new QScrollBar(Qt::Vertical, this);

    auto count = static_cast<int>(_term->scrollBackCount());
    auto screenHeight = _term->terminalSize().height();

    _scrollBar->setRange(0, count);
    _scrollBar->setPageStep(screenHeight);

    auto layout = new QHBoxLayout(this);
    layout->setMargin(0);
    layout->setSpacing(0);
    layout->addWidget(_term, 1);
    layout->addWidget(_scrollBar, 0);
    setLayout(layout);

    connect(_term, &TerminalArea::terminalResized, this, &Terminal::terminalResized);
    connect(_term, &TerminalArea::inputReceived, this, &Terminal::inputReceived);
    connect(_term, &TerminalArea::userInputReceived, this, &Terminal::onUserInput);
    connect(_term, &TerminalArea::titleChanged, this, &Terminal::titleChanged);

    connect(_scrollBar, &QScrollBar::valueChanged, this, &Terminal::onScrollbarValueChanged);
}


void terminal::Terminal::wheelEvent(QWheelEvent *event)
{
    _scrollBar->event(event);
}
