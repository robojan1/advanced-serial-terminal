#ifndef TERMINAL_TERMINAL_H
#define TERMINAL_TERMINAL_H

#include <QWidget>

class QScrollBar;

namespace terminal {

class TerminalArea;

class Terminal : public QWidget
{
    Q_OBJECT
public:
    explicit Terminal(QWidget *parent = nullptr);

    TerminalArea *terminal() { return _term; }
    void setMaxScrollBack(int max);
    int maxScrollBack() const;

    const QString &title() const;

signals:
    void inputReceived(const QByteArray &data);
    void terminalResized(QSize size);
    void titleChanged();

public slots:
    void writeOutput(const QByteArray &data);

protected:
    void wheelEvent(QWheelEvent *event) override;

private slots:
    void onScrollbarValueChanged(int value);
    void onUserInput();

private:
    TerminalArea *_term;
    QScrollBar *_scrollBar;

    void createInstance();
};

}

#endif // TERMINAL_TERMINAL_H
