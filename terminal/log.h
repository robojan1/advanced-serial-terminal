#ifndef TERMINAL_LOG_H
#define TERMINAL_LOG_H

#include <stdarg.h>

namespace terminal {

void log(void *data, const char *file, int line, const char *func,
         const char *subs, unsigned int sev, const char *format,
         va_list args);

}

#endif // TERMINAL_LOG_H
