#include <terminal/vte.h>
#include <terminal/log.h>
#include <stdexcept>
#include <cassert>

terminal::VTE::VTE(terminal::Screen *screen) :
    _vte(nullptr)
{
    int err = tsm_vte_new(&_vte, screen->getInternal(),
                          &terminal::VTE::onWriteStatic, this,
                          &terminal::VTE::onOSCStatic, this,
                          &terminal::log, nullptr);
    if(err < 0) {
        switch(err) {
        case -ENOMEM:
            throw std::bad_alloc();
        case -EINVAL:
            assert(err == 0);
            break;
        default:
            throw std::runtime_error("Unknown error");
        }
    }
}

terminal::VTE::~VTE()
{
    tsm_vte_unref(_vte);
}

void terminal::VTE::onWrite(const char *u8, size_t len)
{
    (void)u8;
    (void)len;
}

void terminal::VTE::onOSC(int cmd, size_t len, const uint32_t *arg)
{
    (void)cmd;
    (void)len;
    (void)arg;
}

void terminal::VTE::onWriteStatic(tsm_vte *vte, const char *u8, size_t len, void *data)
{
    (void)vte;
    terminal::VTE *self = reinterpret_cast<terminal::VTE *>(data);
    self->onWrite(u8, len);
}

void terminal::VTE::onOSCStatic(tsm_vte *vte, int cmd, size_t len, const uint32_t *arg, void *data)
{
    (void)vte;
    terminal::VTE *self = reinterpret_cast<terminal::VTE *>(data);
    self->onOSC(cmd, len, arg);
}
