#include <terminal/terminalarea.h>
#include <terminal/keys.h>
#include <external/xkbcommon-keysyms.h>
#include <QPaintEvent>
#include <QRegion>
#include <QPainter>
#include <QColor>
#include <QGlyphRun>
#include <QDebug>
#include <memory>
#include <QFontMetrics>
#include <QTextLayout>
#include <QFontDatabase>
#include <QClipboard>
#include <QApplication>
#include <QMenu>
#include <QMimeData>
#include <util/system.h>

terminal::TerminalArea::TerminalArea(QWidget *parent) :
    QWidget(parent), Screen(), VTE(this), _age(0), _lastSelectedPos(-1,-1), _hasSelection(false), _rightClickMenu(nullptr),
    _rightClickCopyAction(nullptr), _rightClickPasteAction(nullptr)
{
    createRightClickMenu();
    setFocusPolicy(Qt::StrongFocus);
    setAttribute(Qt::WA_OpaquePaintEvent, true);
    setAttribute(Qt::WA_InputMethodEnabled, true);
    setFont(getFixedFont());
    calculateBlockSize();    
}

terminal::TerminalArea::~TerminalArea()
{

}

QSize terminal::TerminalArea::sizeHint() const
{
    return QSize(Screen::width()*_blockSize.width(), Screen::height()*_blockSize.height());
}

QSize terminal::TerminalArea::terminalSize() const
{
    return QSize(Screen::width(), Screen::height());
}

const QString &terminal::TerminalArea::title() const
{
    return _title;
}

QVariant terminal::TerminalArea::inputMethodQuery(Qt::InputMethodQuery query) const
{
    switch(query) {
    case Qt::ImEnabled: return true;
    case Qt::ImCursorRectangle: {
        return QRect(Screen::cursorX() * _blockSize.width(),
                     Screen::cursorY() * _blockSize.height(),
                     _blockSize.width(), _blockSize.height());
    }
    case Qt::ImFont: return font();
    case Qt::ImCursorPosition: return Screen::cursorX();
    case Qt::ImSurroundingText: return QVariant();
    case Qt::ImCurrentSelection: return QVariant();
    case Qt::ImMaximumTextLength: return QVariant();
    case Qt::ImAnchorPosition: return Screen::cursorX();
    case Qt::ImHints: return Qt::ImhNoAutoUppercase;
    case Qt::ImPreferredLanguage: return QVariant();
    case Qt::ImAbsolutePosition: return QPoint(Screen::cursorX(), Screen::cursorY());
    case Qt::ImTextBeforeCursor: return "";
    case Qt::ImTextAfterCursor: return "";
    case Qt::ImEnterKeyType: return QVariant();
    case Qt::ImAnchorRectangle: return QVariant();
    case Qt::ImInputItemClipRectangle: return QVariant();
    default: return QVariant();

    }
}

void terminal::TerminalArea::writeOutput(const QByteArray &data)
{
    VTE::input(data.data(), data.size());
    update();
}

int terminal::TerminalArea::onDraw(uint32_t id, const uint32_t *ch, size_t len,
                               unsigned int width, unsigned int posx, unsigned int posy,
                               const terminal::ScreenAttribute &attr, tsm_age_t age)
{
    if(!_activePainter) return 0;

    int x = posx * _blockSize.width();
    int y = posy * _blockSize.height();

    int blockWidth = _blockSize.width();
    if(posx == terminalSize().width() - 1) {
        blockWidth += _blockSize.width();
    }
    int blockHeight = _blockSize.height();
    if(posy == terminalSize().height() - 1) {
        blockHeight+= _blockSize.height();
    }

    // Skip if old
    if(_age > 0 && age > 0 && age <= _age && !_activeRegion.intersects(QRect(x,y,blockWidth, blockHeight))) {
        return 0;
    }

    QColor bgColor = QColor::fromRgb(attr.br, attr.bg, attr.bb);
    QColor fgColor = QColor::fromRgb(attr.fr, attr.fg, attr.fb);

    if(attr.inverse) {
        std::swap(bgColor, fgColor);
    }
    QBrush bgBrush(bgColor);

    if(len == 0) {
        _activePainter->fillRect(x, y, blockWidth, blockHeight, bgBrush);
    } else {
        char cBuf[16];
        auto cSize = tsm_ucs4_to_utf8(*ch, cBuf);
        _activePainter->setPen(fgColor);
        _activePainter->setBackground(bgBrush);
        _activePainter->setBackgroundMode(Qt::BGMode::OpaqueMode);
        _activePainter->fillRect(x, y, blockWidth, blockHeight, bgBrush);
        _activePainter->drawText(x, y, _blockSize.width(), _blockSize.height(), Qt::AlignHCenter | Qt::AlignBottom,
                                 QString::fromUtf8(cBuf, cSize));
    }
    return 0;
}

void terminal::TerminalArea::onWrite(const char *u8, size_t len)
{
    inputReceived(QByteArray(u8, len));
}

void terminal::TerminalArea::onOSC(int cmd, size_t len, const uint32_t *arg)
{
    switch(cmd) {
    case 0:
        _title = QString::fromUcs4(arg, len);
        titleChanged();
        break;
    default:
        break;
    }
}

void terminal::TerminalArea::paintEvent(QPaintEvent *event)
{
    _activePainter = std::make_shared<QPainter>(this);
    _activeRegion = event->region();

    QSize activeSize(terminalSize().width() * _blockSize.width(),
                     terminalSize().height() * _blockSize.height());


    _age = Screen::draw();

    _activePainter = nullptr;
}

void terminal::TerminalArea::resizeEvent(QResizeEvent *event)
{
    QSize terminalSize(event->size().width() / _blockSize.width(),
                       event->size().height() / _blockSize.height());
    Screen::resize(terminalSize.width(), terminalSize.height());
    emit terminalResized(terminalSize);
}

void terminal::TerminalArea::keyPressEvent(QKeyEvent *event)
{
    uint32_t keysym = keyToKeySym(static_cast<Qt::Key>(event->key()));
    tsm_vte_modifier keyMod = keyModtoVteMod(event->modifiers());
    QString text = event->text();
    uint32_t unicode = text.size() > 0 ? event->text().toUcs4()[0] : TSM_VTE_INVALID;
    uint32_t ascii = XKB_KEY_NoSymbol;
    if(unicode <= 0x7F && unicode >= 0x20) {
        ascii = unicode;
    }

    //qDebug() << "Keypress event: " << event;

    VTE::handleKeyboard(keysym, ascii, keyMod, unicode);
    userInputReceived();
}

void terminal::TerminalArea::inputMethodEvent(QInputMethodEvent *event)
{
    //qDebug() << "Input method event: " << event;
    if(!event->commitString().isEmpty()) {
        QKeyEvent keyEvent(QEvent::KeyPress, XKB_KEY_NoSymbol, Qt::NoModifier, event->commitString());
        keyPressEvent(&keyEvent);
    }
    event->accept();
}

void terminal::TerminalArea::mousePressEvent(QMouseEvent *event)
{
    if((event->buttons() & Qt::RightButton) != 0) {
        showRightClickMenu(event->pos());
        event->accept();
    } else if((event->buttons() & Qt::LeftButton) != 0) {
        auto mousePos = event->pos();
        QPoint termPos(mousePos.x() / _blockSize.width(), mousePos.y() / _blockSize.height());
        if(termPos.x() >= Screen::width()) termPos.setX(Screen::width() - 1);
        if(termPos.y() >= Screen::height()) termPos.setY(Screen::height() - 1);
        update();
        _lastSelectedPos = termPos;
        _mouseStartPoint = termPos;
        event->accept();
    } else {
        event->ignore();
    }
}

void terminal::TerminalArea::mouseReleaseEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton) {
        if(!_hasSelection) {
            Screen::selectionReset();
            update();
        } else {
            _hasSelection = false;
#if defined(Q_OS_LINUX) || defined(Q_OS_UNIX)
            copyToSelectionBuffer();
#endif
        }
        _lastSelectedPos = QPoint(-1,-1);
        event->accept();
    } else if(event->button() == Qt::MidButton) {
        inputFromSelectionBuffer();
        event->accept();
    } else {
        event->ignore();
    }
}

void terminal::TerminalArea::mouseDoubleClickEvent(QMouseEvent *event)
{
    event->ignore();
}

void terminal::TerminalArea::mouseMoveEvent(QMouseEvent *event)
{
    if((event->buttons() & Qt::LeftButton) != 0) {
        auto mousePos = event->pos();
        QPoint termPos(mousePos.x() / _blockSize.width(), mousePos.y() / _blockSize.height());
        if(termPos.x() >= Screen::width()) termPos.setX(Screen::width() - 1);
        if(termPos.y() >= Screen::height()) termPos.setY(Screen::height() - 1);
        if(!_hasSelection && _lastSelectedPos != termPos) {
            _hasSelection = true;
            Screen::selectionStart(_lastSelectedPos.x(), _lastSelectedPos.y());
        }
        Screen::selectionTarget(termPos.x(), termPos.y());
        if(_lastSelectedPos != termPos) {
            update();
        }
        _lastSelectedPos = termPos;
        event->accept();
    } else {
        event->ignore();
    }
}

void terminal::TerminalArea::calculateBlockSize()
{
    static const char chars[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%*()_+-=[]{}\\|;:'\",./<>?`~";
    QTextLayout layout(chars, font());
    layout.beginLayout();
    auto line = layout.createLine();
    line.setNumColumns(100);
    layout.endLayout();

    double maxCharWidth = 1;

    auto glyphRunList = layout.glyphRuns();
    for(auto &glyphRun : glyphRunList) {
        auto rawFont = glyphRun.rawFont();
        for(auto index : glyphRun.glyphIndexes()) {
            auto glyphRect = rawFont.boundingRect(index);
            if(glyphRect.width() > maxCharWidth) maxCharWidth = glyphRect.width();
        }
    }

    auto metrics = fontMetrics();
    _blockSize = QSize(static_cast<int>(maxCharWidth), metrics.height());
}

void terminal::TerminalArea::showRightClickMenu(QPoint pos)
{
    _rightClickCopyAction->setEnabled(!Screen::selectionCopy().empty());
    _rightClickPasteAction->setEnabled(hasValidClipboardData());
    _rightClickMenu->popup(mapToGlobal(pos));
}

void terminal::TerminalArea::createRightClickMenu()
{
    _rightClickMenu = new QMenu(this);
    _rightClickCopyAction = _rightClickMenu->addAction(tr("Copy"));
    connect(_rightClickCopyAction, &QAction::triggered, [&]() {
        copySelection();
    });

    _rightClickPasteAction = _rightClickMenu->addAction(tr("Paste"));
    connect(_rightClickPasteAction, &QAction::triggered, [&]() {
        inputFromClipboard();
    });
}

void terminal::TerminalArea::copySelection()
{
    std::string selection = Screen::selectionCopy();
    if(selection.size() > 0) {
        QApplication::clipboard()->setText(QString::fromStdString(selection));
    }
}

void terminal::TerminalArea::inputFromClipboard()
{
    if(hasValidClipboardData()) {
        auto text = QApplication::clipboard()->text().toUcs4();
        for(auto c : text) {
            uint32_t ascii = XKB_KEY_NoSymbol;
            if(c <= 0x7F && c >= 0x20) {
                ascii = c;
            }
            VTE::handleKeyboard(XKB_KEY_NoSymbol, ascii, 0, c);
        }
        userInputReceived();
    }
}

bool terminal::TerminalArea::hasValidClipboardData()
{
    auto clipboard = QApplication::clipboard();
    auto mimeData = clipboard->mimeData();
    return mimeData->hasText() && !mimeData->text().isEmpty();
}

void terminal::TerminalArea::copyToSelectionBuffer()
{
    std::string selection = Screen::selectionCopy();
    if(selection.size() > 0) {
        QApplication::clipboard()->setText(QString::fromStdString(selection), QClipboard::Selection);
    }
}

void terminal::TerminalArea::inputFromSelectionBuffer()
{
    auto text = QApplication::clipboard()->text(QClipboard::Selection).toUcs4();
    for(auto c : text) {
        uint32_t ascii = XKB_KEY_NoSymbol;
        if(c <= 0x7F && c >= 0x20) {
            ascii = c;
        }
        VTE::handleKeyboard(XKB_KEY_NoSymbol, ascii, 0, c);
    }
    userInputReceived();
}
