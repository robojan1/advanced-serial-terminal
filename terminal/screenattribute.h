#ifndef TERMINAL_SCREENATTRIBUTE_H
#define TERMINAL_SCREENATTRIBUTE_H

#include <stdint.h>
#include <libtsm.h>

namespace terminal {

using ScreenAttribute = struct tsm_screen_attr;

}

#endif // TERMINAL_SCREENATTRIBUTE_H
