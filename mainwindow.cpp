#include "mainwindow.h"
#include <QMenuBar>
#include <QApplication>
#include <QStatusBar>
#include <QLayout>
#include <libtsm.h>
#include <QDockWidget>
#include <windows/shellterminalwindow.h>
#include <windows/codeeditorwindow.h>
#include <dialogs/serialparametersdialog.h>
#include <settings.h>
#include <engine/engine.h>
#include <engine/vte.h>
#include <QDebug>
#include <QMessageBox>
#include <QDirIterator>
#include <QMap>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), _activeWindow(nullptr)
{
    initialize();
}

MainWindow::~MainWindow()
{
}

void MainWindow::initialize()
{
    createMenus();
    createStatusBar();
    createDockableWindow();

    resize(800,600);

    connect(qApp, &QApplication::focusChanged, this, &MainWindow::onFocusChanged);
}

void MainWindow::createMenus()
{
    createFileMenu();
    createDebugMenu();
}

void MainWindow::createFileMenu()
{
    auto fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(tr("&Exit"), []() {
        QApplication::quit();
    });
    auto fileNewMenu = fileMenu->addMenu(tr("&New"));
    populateNewMenu(fileNewMenu);
    fileNewMenu->addAction(tr("&Code"), [&]() {
        createCodeEditor();
    });
}

void MainWindow::addLuaFileToMenu(QMenu *menu, const QString &path)
{
    QFile f(path);
    if(!f.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "Error could not open file " << path << " : " << f.errorString();
        return;
    }
    QTextStream script(&f);
    // Find the start of the comment block
    while(!script.atEnd()) {
        QString line = script.readLine();
        line = line.trimmed();
        if(line.isEmpty()) continue;
        if(line.startsWith("--")) break;
    }
    // Parse all the data
    QString lastKey;
    QMap<QString, QString> luaInfo;
    while(!script.atEnd()) {
        QString line = script.readLine();
        line = line.trimmed();
        if(line.isEmpty()) break;
        // Remove the comment symbols
        if(!line.startsWith("--")) break;
        auto content = line.midRef(2);
        content = content.trimmed();
        if(content.isEmpty()) {
            lastKey.clear();
            continue;
        }

        // Split the text in key value pairs
        auto seperatorIdx = content.indexOf(":");
        if(seperatorIdx < 0) {
            if(lastKey.isEmpty()) continue;
            luaInfo[lastKey] += " " + content;
        } else {
            auto key = content.left(seperatorIdx).toString().toLower();
            auto value = content.mid(seperatorIdx + 1).trimmed();
            luaInfo[key] = value.toString();
            lastKey = key;
            qDebug() << key << ": " << value;      
        }

    }

    QFileInfo info(path);
    QString name = luaInfo.contains("name") ? luaInfo["name"] : info.baseName(); 


    QAction *action = menu->addAction(name);
    action->setData(path);
    connect(action, &QAction::triggered, [action, this](bool checked) {
        Q_UNUSED(checked);

        //QAction *action = qobject_cast<QAction *>(QObject::sender());
        auto path = action->data().toString();
        QFile file(path);
        if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            QMessageBox errorDialog(QMessageBox::Icon::Critical, "Error", QString("Could not open file %1").arg(path), QMessageBox::Ok);
            errorDialog.exec();
            return;
        }
        auto src = QString::fromUtf8(file.readAll());
        runScript(src);
    });
}

bool MainWindow::populateMenuFromDirectory(QMenu *menu, const QString &path)
{
    QDir dir(path);
    QDirIterator iter(path, QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot);
    bool hasEntries = false;
    while(iter.hasNext())
    {
        QString filePath = iter.next();
        auto info = iter.fileInfo();
        if(info.isDir()) {
            QMenu *subMenu = new QMenu(iter.fileName());
            bool dirHasEntries =populateMenuFromDirectory(subMenu, filePath);
            hasEntries |= dirHasEntries;
            if(dirHasEntries) {
                subMenu->setParent(menu);
                menu->addMenu(subMenu);
            } else {
                subMenu->deleteLater();
            }
        } else if(info.isFile() && info.completeSuffix().compare("lua", Qt::CaseInsensitive) == 0) {
            hasEntries = true;
            addLuaFileToMenu(menu, filePath);
        }
    }
    return hasEntries;
}

void MainWindow::populateNewMenu(QMenu *menu)
{
    auto systemScriptsPath = Settings::Get().systemScriptsDir();
    bool addedScripts = false;
    if(!systemScriptsPath.isEmpty()) {
        addedScripts |= populateMenuFromDirectory(menu, systemScriptsPath);
    }
    if(!addedScripts) {
        menu->addAction("No scripts loaded")->setEnabled(false);
    }
}

void MainWindow::createStatusBar()
{
    auto bar = statusBar();
}

void MainWindow::createCodeEditor()
{
    auto window = new windows::CodeEditorWindow(this);

    addWindow(window);
}

void MainWindow::createDockableWindow()
{
    // Create empty center widget
    auto centerWidget = new QWidget(this);
    centerWidget->setMaximumHeight(0);
    setCentralWidget(centerWidget);

    // Add default widget
    qRegisterMetaType<QDockWidget::DockWidgetFeatures>();
    //setTabPosition(Qt::AllDockWidgetAreas, QTabWidget::North);
}

void MainWindow::runScript(const QString &script)
{
    auto ses = new Session(this);
    connect(ses, &Session::done, [&]() {
        cleanupSessions();
    });

    ses->setAddWindowCallback(std::bind(&MainWindow::addWindow, this, std::placeholders::_1));

    try
    {
        _sessions.insert(ses);
        ses->run(script.toUtf8().data());
    }
    catch (std::exception &ex)
    {
        QMessageBox errorDialog(QMessageBox::Icon::Critical, "Error", ex.what(), QMessageBox::Ok);
        errorDialog.exec();
        ses->deleteLater();
    }
}

void MainWindow::createDebugMenu()
{
    auto menu = menuBar()->addMenu(tr("&Debug"));
    connect(menu, &QMenu::aboutToShow, [&]() {
        updateDebugMenu();
    });
    _debugRunAction = menu->addAction(tr("&Run"), [&]() {
        windows::CodeEditorWindow *ce = qobject_cast<windows::CodeEditorWindow *>(_activeWindow);
        if(ce) {
            runScript(ce->text());
        }
    });
}

void MainWindow::addWindow(windows::BaseWindow *window)
{
    //term->setAllowedAreas(Qt::TopDockWidgetArea|Qt::BottomDockWidgetArea);
    addDockWidget(Qt::TopDockWidgetArea, window);
    window->setAttribute(Qt::WA_DeleteOnClose);

    _windows.insert(window);

    connect(window, &windows::BaseWindow::windowClosing, this, &MainWindow::onSubWindowClosing, Qt::DirectConnection);

    if (_activeWindow == nullptr)
    {
        _activeWindow = window;
    }
}

void MainWindow::updateDebugMenu()
{
    bool isExecutable = _activeWindow != nullptr &&
                        _activeWindow->windowType() == windows::BaseWindowTypes::CodeEditorWindow;
    _debugRunAction->setEnabled(isExecutable);
}

void MainWindow::onSubWindowClosing(windows::BaseWindow *window)
{
    _windows.erase(window);
    if (_activeWindow == window)
    {
        if (_windows.empty())
        {
            _activeWindow = nullptr;
        }
        else
        {
            _activeWindow = *_windows.begin();
        }
    }
}

static windows::BaseWindow *getBaseWindowFromWidget(QWidget *widget)
{
    QObject *obj = widget;
    const char *baseWindowName = windows::BaseWindow::staticMetaObject.className();
    while (obj != nullptr && !obj->inherits(baseWindowName))
    {
        obj = obj->parent();
    }
    return qobject_cast<windows::BaseWindow *>(obj);
}

void MainWindow::onFocusChanged(QWidget *oldWidget, QWidget *newWidget)
{
    Q_UNUSED(oldWidget);
    windows::BaseWindow *newWindow = getBaseWindowFromWidget(newWidget);
    if (newWindow != nullptr && _activeWindow != newWindow)
    {
        _activeWindow = newWindow;
    }
}

void MainWindow::cleanupSessions()
{
    for (auto it = _sessions.begin(); it != _sessions.end();)
    {
        if ((*it)->isDone())
        {
            (*it)->deleteLater();
            it = _sessions.erase(it);
        }
        else
        {
            ++it;
        }
    }
}
