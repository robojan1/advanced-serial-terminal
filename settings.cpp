#include "settings.h"
#include <QApplication>
#include <QDir>
#include <QProcessEnvironment>

Settings &Settings::Get()
{
    static Settings self;
    return self;
}

void Settings::setShell(QString shell)
{
    if (_shell != shell)
    {
        _shell = shell;
        shellChanged();
    }
}

Settings::Settings() : _shell(QStringLiteral("/bin/bash"))
{
}

QString Settings::systemScriptsDir() const
{
    auto appDir = QDir(QApplication::applicationDirPath() + QDir::separator() + "scripts");

    if(appDir.exists()) {
        return appDir.path();
    }
    auto workingDir = QDir("scripts");
    if(workingDir.exists()) {
        return workingDir.absolutePath();
    }
    auto systemDir = QDir("/usr/share/serteradv/scripts");
    if(systemDir.exists()) {
        return systemDir.path();
    }
    auto systemLocalDir = QDir("/usr/local/share/serteradv/scripts");
    if(systemLocalDir.exists()) {
        return systemLocalDir.path();
    }
    auto env = QProcessEnvironment::systemEnvironment();
    if(env.contains("STA_SRC_DIR")) {
        auto sta_src_dir = env.value("STA_SRC_DIR");
        auto srcDir = QDir(sta_src_dir + QDir::separator() + "scripts");
        if(srcDir.exists()){
            return srcDir.absolutePath();
        }
    }
    return "";
}
