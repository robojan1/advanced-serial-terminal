#ifndef INPUTS_SHELL_H
#define INPUTS_SHELL_H

#include <QObject>
#include <QIODevice>
#include <QFuture>
#include <queue>
#include <mutex>
#include <QSize>

namespace inputs {


class Shell : public QIODevice
{
    Q_OBJECT
public:
    explicit Shell(QObject *parent = nullptr);
    virtual ~Shell();

    virtual bool open(const QString &shell = QStringLiteral("/bin/bash"));
    void close() override;

    static QString defaultShell();


signals:
    void exited(bool normal, int status);

public slots:
    void resize(QSize size);

protected:
    qint64 readData(char *data, qint64 maxlen) override;
    qint64 writeData(const char *data, qint64 len) override;

private slots:
    void onSigChild(int status, pid_t pid, uid_t uid, int code, clock_t utime, clock_t stime);

private:
    pid_t _childPid;
    int _masterFd;
    QFuture<void> _ioPollingFuture;
    bool _running;
    std::queue<char> _rxBuffer;
    std::mutex _rxBufferMutex;

    bool spawnProcess(const QString &shell);
    void ioPollerTask();

    void kill(int timeout = 10);


    bool open(QIODevice::OpenMode mode) override;
};

}

#endif // INPUTS_SHELL_H
