#include "shell.h"
#include <pty.h>
#include <QDebug>
#include <QtConcurrent>
#include <sys/select.h>
#include <sys/signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <iostream>
#include <algorithm>
#include <util/unixsignalhandler.h>
#include <QProcessEnvironment>

using namespace inputs;

Shell::Shell(QObject *parent) :
    QIODevice(parent), _childPid(-1), _masterFd(-1), _running(false)
{

    auto signalHandler = UnixSignalHandler::get();

    // Register handler for the sigchild
    signalHandler->connect(signalHandler, &UnixSignalHandler::signalChild, this, &Shell::onSigChild);
}

Shell::~Shell()
{
    close();
}

bool Shell::open(const QString &shell)
{
    if(isOpen()) return true;
    return spawnProcess(shell);
}

void Shell::close()
{
    if(!isOpen()) return;

    kill();

    _running = false;
    ::close(_masterFd);
    _masterFd = -1;
    _ioPollingFuture.waitForFinished();
}

void Shell::resize(QSize size)
{
    if(!isOpen()) return;
    struct winsize winSize = {
        .ws_row = static_cast<unsigned short>(size.height()),
        .ws_col = static_cast<unsigned short>(size.width())
    };
    if(ioctl(_masterFd, TIOCSWINSZ, &winSize) < 0) {
        throw std::runtime_error("Could not set the terminal size");
    }
    ::kill(_childPid, SIGWINCH);
}

bool Shell::spawnProcess(const QString &shell)
{
    int fd;
    qDebug() << "Master TTY: " ;

    pid_t pid = forkpty(&fd, nullptr, nullptr, nullptr);
    if(pid < 0) {
        // Error
        setErrorString(QString("Error could not fork the process: %1").arg(errno));
        return false;
    } else if(pid == 0) {
        // Child process
        setsid();
        const char *executable = shell.toUtf8().data();
        setenv("TERM", "xterm-256color", 1);
        unsetenv("COLUMNS");
        unsetenv("LINES");
        execl(executable, executable, NULL);
        std::cerr << "Error spawning child process: " << errno << std::endl;
        abort();
    } else {
        // Parent process
        _childPid = pid;
        _masterFd = fd;
        setOpenMode(QIODevice::ReadWrite);
        _ioPollingFuture = QtConcurrent::run(std::bind(&Shell::ioPollerTask, this));
    }
    return true;
}

void Shell::ioPollerTask()
{
    std::array<char, 1024> buffer;
    _running = true;
    while(_running) {
        ssize_t len = ::read(_masterFd, buffer.data(), buffer.size());
        if(len > 0) {
            {
                std::lock_guard<std::mutex> guard(_rxBufferMutex);
                for(int i = 0; i < len; i++) {
                    _rxBuffer.push(buffer.at(i));
                }
            }
            readyRead();
        }
    }
}

void Shell::kill(int timeout)
{
    int status = 0;
    if(::waitpid(_childPid, &status, WNOHANG) > 0 && WIFEXITED(status)) {
        return;
    }

    auto signalHandler = UnixSignalHandler::get();
    // Register handler for the sigalarm
    signalHandler->enableSignal(SIGALRM);

    ::kill(_childPid, SIGHUP);
    ::alarm(static_cast<unsigned int>(timeout));
    ::waitpid(_childPid, &status, 0);
    ::alarm(0);
    signalHandler->disableSignal(SIGALRM);
    if(WIFEXITED(status)) {
        return;
    }
    ::kill(_childPid, SIGKILL);
    ::waitpid(_childPid, &status, 0);
}

bool Shell::open(QIODevice::OpenMode mode)
{
    Q_UNUSED(mode);
    setErrorString("Invalid function call");
    return false;
}

qint64 Shell::readData(char *data, qint64 maxlen)
{
    qint64 len = std::min(maxlen, static_cast<qint64>(_rxBuffer.size()));
    {
        std::lock_guard<std::mutex> guard(_rxBufferMutex);
        for(int i = 0; i < len; i++) {
            data[i] = _rxBuffer.front();
            _rxBuffer.pop();
        }
    }
    return len;
}

qint64 Shell::writeData(const char *data, qint64 len)
{
    return ::write(_masterFd, data, len);
}

void Shell::onSigChild(int status, pid_t pid, uid_t uid, int code, clock_t utime, clock_t stime)
{
    Q_UNUSED(uid);
    Q_UNUSED(utime);
    Q_UNUSED(stime);

    if(pid == _childPid && (code == CLD_EXITED || code == CLD_KILLED || code == CLD_DUMPED)) {
        // Child exited
        exited(code == CLD_EXITED, status);
    }
}


QString Shell::defaultShell()
{
    return QProcessEnvironment::systemEnvironment().value("SHELL", "/bin/sh");
}
