#include <engine/engine.h>
#include <engine/error.h>
#include <new>
#include <stdlib.h>
#include <iostream>
#include <cassert>

namespace constants {
    inline constexpr char self[] = "sta.self";
    inline constexpr char refTable[] = "sta.reftable";
}

using namespace lua;

std::shared_ptr<Engine> Engine::make_engine(bool loadDefaultLibs)
{
    return std::shared_ptr<Engine>{new Engine(loadDefaultLibs)};
}

Engine::Engine(bool loadDefaultLibs) : _funcIdCounter(0), _refCounter(0)
{
    _state = lua_newstate(Engine::allocator, this);
    if(_state == nullptr) throw std::bad_alloc();

    if(loadDefaultLibs) {
        this->loadDefaultLibs();
    }

    createEngineType();

    createReferenceTabel();
}

Engine::~Engine()
{
    printf("Destroying lua engine\n");
    if(_state) {
        destroy();
    }
}

void Engine::destroy()
{
    if(_state)
    {
        lua_close(_state);
        _state = nullptr;
    }
}

void Engine::loadDefaultLibs()
{
    luaL_openlibs(_state);
}

void Engine::doString(const char *str)
{
    int err;

    lua_pushcfunction(_state, &Engine::errorHandler);
    check_lua_error(luaL_loadstring(_state, str));
    try {
        check_lua_error(lua_pcall(_state, 0, 0, -2));
        lua_pop(_state, 1);
    } catch(lua::error) {
        lua_pop(_state, 1);
        throw;
    }
}

void Engine::doFile(const char *file)
{
    int err;

    lua_pushcfunction(_state, &Engine::errorHandler);
    check_lua_error(luaL_loadfile(_state, file));
    try {
        check_lua_error(lua_pcall(_state, 0, 0, -2));
        lua_pop(_state, 1);
    } catch(lua::error) {
        lua_pop(_state, 1);
        throw;
    }
}

void *Engine::allocator(void *ud, void *ptr, size_t osize, size_t nsize)
{
    auto *self = reinterpret_cast<Engine *>(ud);
    (void)self;
    (void)osize;
    if(nsize == 0) {
        // printf("Freeing 0x%p %u\n", ptr, osize);
        free(ptr);
        return nullptr;
    } else {
        // printf("Allocating 0x%p %u\n", ptr, osize);
        return realloc(ptr, nsize);
    }
}

int Engine::errorHandler(lua_State *L)
{
    int nargs = lua_gettop(L);
    (void) nargs;
    return 1;
}

int Engine::functionCallback(lua_State *L)
{
    // Get the engine object
    Engine *self = getEngine(L);
    if(self == nullptr) {
        luaL_error(L, "Engine object was not set. Cannot handle function calls");
    }

    //int selfIdx = lua_upvalueindex(1);
    int funcIdIdx = lua_upvalueindex(2);
    //LuaEngine * self2 = (LuaEngine *)lua_touserdata(L, selfIdx);
    if(!lua_isnumber(L, funcIdIdx)) {
        luaL_error(L, "No function id registered for the c function");
    }
    int64_t funcId = lua_tointeger(L, funcIdIdx);

    auto f = self->_userFunctionMap.find(funcId);
    if(f == self->_userFunctionMap.end()) {
        luaL_error(L, "Cannot find the registered user function");
    }

    try {
        auto &o = f->second;
        int nResults = o.callback(self);
        if(self->stackSize() != nResults) {
            self->dumpStack();
            assert(self->stackSize() == nResults);
        }
        return nResults;
    } catch(std::exception &ex) {
        luaL_error(L, "%s", ex.what());
        return 0;
    }
}

void Engine::dumpStack() const {
    int n = lua_gettop(_state);
    std::cout << "Stack: " << std::endl;
    std::cout << "-------" << std::endl;

    for(int i = 1; i <= n; i++) {
        int type = lua_type(_state, i);
        switch(type) {
        case LUA_TSTRING:
            std::cout << "\"" << lua_tostring(_state, i) << "\"" << std::endl;
            break;
        case LUA_TBOOLEAN:
            std::cout << (lua_toboolean(_state, i) ? "true" : "false") << std::endl;
            break;
        case LUA_TNUMBER:
            std::cout <<  lua_tonumber(_state, i) << std::endl;
            break;
        default:
            std::cout << lua_typename(_state, type) << std::endl;
            break;
        }
        std::cout << "-------" << std::endl;
    }    
}

int Engine::stackSize() const
{
    return lua_gettop(_state);
}

void Engine::createEngineType()
{
    // Store this in the lua state at 
    lua_pushstring(_state, constants::self);
    lua_pushlightuserdata(_state, this);
    lua_settable(_state, LUA_REGISTRYINDEX);    
}

Engine *Engine::getEngine(lua_State *L)
{
    lua_pushstring(L, constants::self);
    lua_gettable(L, LUA_REGISTRYINDEX);
    Engine *result = reinterpret_cast<Engine *>(lua_touserdata(L,-1));
    lua_pop(L, 1);
    return result;
}

lua::Table Engine::createTable(int narr, int nrec)
{
    lua_createtable(_state, narr, nrec);

    return lua::Table(shared_from_this(), lua_absindex(_state, -1));
}

lua::Table Engine::getMetaTable(int idx)
{
    if(lua_getmetatable(_state, idx) == 0) {
        // No metatable
        throw std::invalid_argument("Type doesn't have a metatable");
    }
    int tableIdx = lua_absindex(_state, -1);
    return lua::Table(shared_from_this(), tableIdx);
}

void Engine::setMetaTable(int idx, lua::Table &&table)
{
    // Table should be at the top of the stack
    assert(lua_gettop(_state) == table.stackIdx());
    table.reset();

    // Set the metatable
    lua_setmetatable(_state, idx);
}

void Engine::pushValue(const char *value)
{
    lua_pushstring(_state, value);
}

void Engine::pushValue(const std::string &value)
{
    lua_pushstring(_state, value.c_str());
}

void Engine::pushValue(double value)
{
    lua_pushnumber(_state, value);
}

void Engine::pushValue(int value)
{
    lua_pushinteger(_state, value);
}

void Engine::pushValue(int64_t value)
{
    lua_pushinteger(_state, value);
}

void Engine::pushValue(bool value)
{
    lua_pushboolean(_state, value ? 1 : 0);
}

void Engine::pushValue(std::function<int(Engine *)> func, std::string name)
{
    int64_t id = ++_funcIdCounter;

    // Create the general callback function
    lua_pushlightuserdata(_state, this);
    lua_pushinteger(_state, id);
    lua_pushcclosure(_state, Engine::functionCallback, 2);

    // Store the callback function
    UserFunction o = {
        .callback = func,
        .name = std::move(name)
    };
    _userFunctionMap[id] = std::move(o);

    // TODO: a method for removing functions
}

void Engine::pushValue(int (*value)(Engine *), const char *name)
{
    pushValue(std::bind(value, std::placeholders::_1), name);
}

void Engine::pushValue(Table &&value)
{
    // Table should be at the top of the stack
    assert(lua_gettop(_state) == value.stackIdx());
    value.reset();
}

void Engine::pushValue(Value &&value)
{
    // Value should be at the top of the stack
    assert(lua_gettop(_state) == value.stackIdx());
    value.reset();
}

void Engine::pushValue(const Table &value)
{
    lua_pushnil(_state);
    lua_copy(_state, value.stackIdx(), -1);
}

void Engine::pushValue(const Value &value)
{
    lua_pushnil(_state);
    lua_copy(_state, value.stackIdx(), -1);
}

void Engine::pushValue(void *value) 
{
    lua_pushlightuserdata(_state, value);
}

void Engine::pushNil()
{
    lua_pushnil(_state);
}

void Engine::storeGlobal(const std::string &name, Table &&value)
{
    // Table should be at the top of the stack
    assert(lua_gettop(_state) == value.stackIdx());
    value.reset();

    // Register the global
    lua_setglobal(_state, name.c_str());
}

Value Engine::getArgument(int idx) {
    int nArgs = stackSize();
    if(idx <= 0 || idx > nArgs) {
        throw std::out_of_range("LUA stack does not contain specified index");
    }

    return Value(shared_from_this(), lua_absindex(_state, idx));
}

Value Engine::getValue(int idx) {
    idx = lua_absindex(_state, idx);
    int nArgs = stackSize();
    if(idx <= 0 || idx > nArgs) {
        throw std::out_of_range("LUA stack does not contain specified index");
    }
    return Value(shared_from_this(), idx);
}

std::string Engine::checkString(int idx)
{
    return luaL_checkstring(_state, idx);
}

int64_t Engine::checkInteger(int idx)
{
    return luaL_checkinteger(_state, idx);
}

void Engine::callStackFunction(int nargs, int nresults)
{
    lua_call(_state, nargs, nresults);
}

void Engine::protectedCallStackFunction(int nargs, int nresults)
{
    check_lua_error(lua_pcall(_state, nargs, nresults, 0));
}

void Engine::pop(int n) {
    lua_pop(_state, n);
}

void Engine::createReferenceTabel()
{
    lua_createtable(_state, 0, 0);
    lua_setfield(_state, LUA_REGISTRYINDEX, constants::refTable);
}

int64_t Engine::storeReference(Value &&value)
{
    assert(lua_gettop(_state) == value.stackIdx());
    lua_getfield(_state, LUA_REGISTRYINDEX, constants::refTable);
    lua_insert(_state, -2);
    lua_seti(_state, -2, ++_refCounter);
    lua_pop(_state, 1);
    return _refCounter;
}

Value Engine::getReference(int64_t id)
{
    lua_getfield(_state, LUA_REGISTRYINDEX, constants::refTable);
    lua_geti(_state, -1, id);
    lua_insert(_state, -2);
    lua_pop(_state, 1);
    int index = lua_absindex(_state, -1);
    return Value(shared_from_this(), index);
}

void Engine::removeReference(int64_t id)
{
    lua_getfield(_state, LUA_REGISTRYINDEX, constants::refTable);
    pushNil();
    lua_seti(_state, -2, id);
    pop();
}
