#include <engine/engine.h>
#include <cassert>

using namespace lua;

Value::Value(std::shared_ptr<Engine> engine, int pos) :
    _stackIdx(pos), _engine(std::move(engine))
{
}

Value::Value(Value &&other)
{
    other._stackIdx = _stackIdx;
    other._engine = _engine;
    _stackIdx = 0;
}

Value::~Value()
{
}

void Value::destroy()
{
    lua_pop(_engine->internalState(), 1);
    _stackIdx = 0;
}

void Value::reset()
{
    _stackIdx = 0;
}

bool Value::isNil() const
{
    return lua_isnil(_engine->internalState(), _stackIdx);
}

bool Value::isBoolean() const
{
    return lua_isboolean(_engine->internalState(), _stackIdx);
}

bool Value::isNumber() const
{
    return lua_isnumber(_engine->internalState(), _stackIdx);
}

bool Value::isString() const
{
    return lua_isstring(_engine->internalState(), _stackIdx);
}

bool Value::isFunction() const
{
    return lua_isfunction(_engine->internalState(), _stackIdx);
}

bool Value::isCFunction() const
{
    return lua_iscfunction(_engine->internalState(), _stackIdx);
}

bool Value::isUserData() const
{
    return lua_isuserdata(_engine->internalState(), _stackIdx);
}

bool Value::isThread() const
{
    return lua_isthread(_engine->internalState(), _stackIdx);
}

bool Value::isTable() const
{
    return lua_istable(_engine->internalState(), _stackIdx);
}

std::string Value::toString() const {
    return lua_tostring(_engine->internalState(), _stackIdx);
}

void *Value::toUserData() const {
    return lua_touserdata(_engine->internalState(), _stackIdx);
}

int64_t Value::toInteger() const {
    return lua_tointeger(_engine->internalState(), _stackIdx);
}

Table Value::toTable() {
    Table result(_engine, _stackIdx);
    reset();
    return result;
}