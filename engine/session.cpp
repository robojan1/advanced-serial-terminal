#include <engine/session.h>
#include <windows/basewindow.h>
#include <engine/vte.h>
#include <engine/shell.h>
#include <engine/dialogs.h>
#include <engine/serial.h>

Session::Session(QObject *parent) : 
    QObject(parent), _engine(lua::Engine::make_engine(true))
{
    setupEngine();
} 

Session::Session(Session &&other) : 
    QObject(other.parent()), _engine(std::move(other._engine)), 
    _activeWindows(std::move(other._activeWindows)),
    _addWindowCB(other._addWindowCB)
{
    setupEngine();
}

Session::~Session()
{
    _engine->destroy();
    for(auto window : _activeWindows) {
        window->deleteLater();
    }
}

void Session::run(const std::string &script)
{
    _engine->doString(script.c_str());
    if(isDone()) {
        done();
    }
}

void Session::runFile(const std::string &file)
{
    _engine->doFile(file.c_str());
    if(isDone()) {
        done();
    }
}

bool Session::isDone() const
{
    return _activeWindows.empty();
}

void Session::setAddWindowCallback(std::function<void(windows::BaseWindow*)> addWindow)
{
    _addWindowCB = std::move(addWindow);
}

int Session::L_addWindow(lua::Engine *engine)
{
    if(engine->stackSize() != 1) {
        luaL_error(engine->internalState(), "Invalid number of arguments");
    }
    if(!_addWindowCB) {
        luaL_error(engine->internalState(), "add window function is not defined");
    }

    // Get the first argument
    auto a1 = engine->getArgument(1);
    if(!a1.isTable()) {
        luaL_argerror(engine->internalState(), 1, "Argument must be userdata");
    }
    // Convert the value into a table
    auto t = a1.toTable();

    // Get the metatable
    auto mt = engine->getMetaTable(1);

    // Get the dock window function
    auto dwFunc = mt["__dock_window"];
    if(!dwFunc.isCFunction()) {
        luaL_argerror(engine->internalState(), 1, "dock_window must be a c function");
    }

    // Get the self pointer
    engine->pushValue(std::move(t["self"]));
    
    // Call the dock window function with the self pointer. It returns a pointer to a base window object
    engine->callStackFunction(1,1);

    // Get the base window object
    auto winVal = engine->getValue(-1);
    assert(winVal.isUserData());
    windows::BaseWindow *win = reinterpret_cast<windows::BaseWindow *>(winVal.toUserData());

    // Do the action
    _addWindowCB(win);

    _activeWindows.insert(win);
    connect(win, &windows::BaseWindow::windowClosing, this, &Session::onWindowClosing);

    // Cleanup
    engine->pop(3);
    return 0;
}

void Session::setupEngine()
{
    _engine->storeGlobal("add_window", 
        std::bind(&Session::L_addWindow, this, std::placeholders::_1));


    lua::VTE::registerLua(_engine.get());
    lua::Shell::registerLua(_engine.get());
    lua::dialogs::registerLua(_engine.get());
    lua::Serial::registerLua(_engine.get());
}

void Session::onWindowClosing(windows::BaseWindow *window)
{
    _activeWindows.erase(window);
    if(isDone()) {
        done();
    }
}
