#ifndef ENGINE_SERIALSETTINGSDIALOG_H
#define ENGINE_SERIALSETTINGSDIALOG_H


#include <QObject>
#include <dialogs/serialparametersdialog.h>
#include <engine/engine.h>
#include <unordered_set>

namespace lua {
    namespace dialogs {
        void registerLua(Engine *engine);


        int L_showSerialSettings(Engine *engine);
    };
}

#endif

