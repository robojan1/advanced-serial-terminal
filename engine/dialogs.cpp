#include <engine/engine.h>
#include <engine/dialogs.h>
#include <dialogs/serialparametersdialog.h>

using namespace lua;

void dialogs::registerLua(Engine *engine) {
    auto t = engine->createTable();
    t.insert("showSerialSettings", dialogs::L_showSerialSettings);

    engine->storeGlobal("dialogs", std::move(t));
}

int dialogs::L_showSerialSettings(Engine *engine)
{
    if(engine->stackSize() != 0) {
        luaL_error(engine->internalState(), "Invalid number of arguments");
    }

    SerialParametersDialog dialog;

    QDialog::DialogCode result = static_cast<QDialog::DialogCode>(dialog.exec());

    engine->pushValue(result == QDialog::Accepted);
    if(result == QDialog::Accepted)
    {
        auto settings = dialog.settings();
        auto t = engine->createTable(0, 6);
        t.insert("device", settings.device().toStdString());
        t.insert("parity", settings.parityString().toStdString());
        t.insert("baudrate", static_cast<int64_t>(settings.baudrate()));
        t.insert("data", settings.dataBits());
        t.insert("stop", settings.stopBits());
        t.insert("flowControl", settings.flowControlString().toStdString());
        t.reset();
    } else {
        engine->pushNil();
    }

    return 2;
}
