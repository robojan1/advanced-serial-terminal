#ifndef ENGINE_SHELL_H
#define ENGINE_SHELL_H

#include <QObject>
#include <memory>
#include <engine/engine.h>
#include <inputs/shell.h>
#include <unordered_set>

namespace lua {

    class Shell : public QObject
    {
        Q_OBJECT
    public:
        Shell(std::weak_ptr<Engine> engine, QString shell);
        ~Shell();
        static void registerLua(Engine *engine);

    public slots:
        void write(const std::string &data);
        void resize(QSize size);

    private slots:
        void onReadyRead();
        void onExited(bool normal, int status);

    private:
        static int L_new(Engine *engine);
        static int L_registerDataCallback(Engine *engine);
        static int L_registerExitCallback(Engine *engine);
        static int L_write(Engine *engine);
        static int L_resize(Engine *engine);
        static Shell *CheckObject(Engine *engine, int idx);
        void removeCallbacks();

        std::weak_ptr<Engine> _engine;
        std::unordered_set<int64_t> _onDataCallbacks;
        std::unordered_set<int64_t> _onExitedCallbacks;

        inputs::Shell *_shell;
    };

}

#endif
