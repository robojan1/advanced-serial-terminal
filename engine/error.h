#ifndef ENGINE_ERROR
#define ENGINE_ERROR

#include <stdexcept>
#include <lua.h>

namespace lua {

    class error : public std::runtime_error
    {
    public:
        using std::runtime_error::runtime_error;
        explicit error(lua_State *L) : std::runtime_error(lua_tostring(L, -1)) { }
    };

    class syntax_error : public error
    {
    public:
        using error::error;
    };

    class memory_error : public error
    {
    public:
        using error::error;
    };

    class garbage_collector_error : public error
    {
    public:
        using error::error;
    };

    class runtime_error : public error
    {
    public:
        using error::error;
    };
}

#endif