
#include <engine/engine.h>
#include <cassert>

using namespace lua;

Table::Table(std::shared_ptr<Engine> engine, int pos) :
    _stackIdx(pos), _engine(std::move(engine))
{
    assert(lua_istable(_engine->internalState(), _stackIdx));
}

Table::Table(Table &&other)
{
    other._stackIdx = _stackIdx;
    other._engine = _engine;
    _stackIdx = 0;
}

Table::~Table()
{
}

void Table::destroy()
{
    lua_pop(_engine->internalState(), 1);
    _stackIdx = 0;
}

void Table::reset()
{
    _stackIdx = 0;
}