#ifndef SERIAL_H
#define SERIAL_H

#include <QObject>
#include <memory>
#include <engine/engine.h>
#include <QSerialPort>
#include <settings/serialsettings.h>
#include <unordered_set>

namespace lua {

class Serial : public QObject
{
    Q_OBJECT
public:
    explicit Serial(std::weak_ptr<Engine> engine, const SerialSettings &settings);
    ~Serial();
    static void registerLua(Engine *engine);

    SerialSettings settings() const;

signals:

public slots:
    void write(const QByteArray &data);

private slots:
    void onDataReady();

private:
    std::weak_ptr<Engine> _engine;
    QSerialPort *_serialPort;
    SerialSettings _settings;
    std::unordered_set<int64_t> _dataCallbacks;

    static int L_new(Engine *engine);
    static int L_write(Engine *engine);
    static int L_registerDataCallback(Engine *engine);
    static int L_getSettings(Engine *engine);
    static int L_setBaudRate(Engine *engine);
    static int L_setDataBits(Engine *engine);
    static int L_setStopBits(Engine *engine);
    static int L_setParity(Engine *engine);
    static int L_setFlowControl(Engine *engine);
    static Serial *CheckObject(Engine *engine, int idx);
    void removeCallbacks();
};

}

#endif // SERIAL_H
