#include "serial.h"
#include <QDebug>

using namespace lua;

Serial::Serial(std::weak_ptr<Engine> engine, const SerialSettings &settings): 
    _engine(std::move(engine)), _serialPort(nullptr), _settings(settings)
{
    _serialPort = new QSerialPort(settings.device(), this);
    _serialPort->setBaudRate(settings.baudrate());
    _serialPort->setDataBits(settings.dataBits());
    _serialPort->setFlowControl(settings.flowControl());
    _serialPort->setParity(settings.parity());
    _serialPort->setFlowControl(settings.flowControl());

    // Lock
    auto enginePtr = _engine.lock();
    if(!_serialPort->open(QIODevice::ReadWrite))
    {
        char *errorStr = _serialPort->errorString().toLocal8Bit().data();
        luaL_error(enginePtr->internalState(), "Could not open serial port: %s", errorStr);
    }

    connect(_serialPort, &QSerialPort::readyRead, this, &Serial::onDataReady);
}

Serial::~Serial()
{
    removeCallbacks();
}

void Serial::registerLua(Engine *engine)
{
    auto t = engine->createTable();
    t.insert("new", Serial::L_new);
    t.insert("registerDataCallback", Serial::L_registerDataCallback);
    t.insert("write", Serial::L_write);
    t.insert("getSettings", Serial::L_getSettings);
    t.insert("setBaudRate", Serial::L_setBaudRate);
    t.insert("setDataBits", Serial::L_setDataBits);
    t.insert("setStopBits", Serial::L_setStopBits);
    t.insert("setParity", Serial::L_setParity);
    t.insert("setFlowControl", Serial::L_setFlowControl);

    engine->storeGlobal("serial", std::move(t));
}

SerialSettings Serial::settings() const 
{
    SerialSettings settings;
    settings.setDevice(_serialPort->portName());
    settings.setBaudrate(_serialPort->baudRate());
    settings.setDataBits(_serialPort->dataBits());
    settings.setStopBits(_serialPort->stopBits());
    settings.setParity(_serialPort->parity());
    settings.setFlowControl(_serialPort->flowControl());
    return settings;
}

void Serial::write(const QByteArray &data)
{
    _serialPort->write(data);
}

void Serial::onDataReady()
{    
    if (_engine.expired())
        return;
    auto data = _serialPort->readAll();
    auto engine = _engine.lock();
    for (auto refId : _dataCallbacks)
    {
        auto f = engine->getReference(refId);
        engine->pushValue(std::string(data.constData(), data.size()));
        try
        {
            engine->callStackFunction(1, 0);
        }
        catch (lua::error &e)
        {
            qWarning() << "Error calling data callback function: " << e.what();
        }
    }
}

Serial *Serial::CheckObject(Engine *engine, int idx)
{
    Value a(engine->shared_from_this(), idx);
    if (a.isTable())
    {
        Table t = a.toTable();
        auto self = t["self"];
        auto mt = engine->getMetaTable(-1);
        auto nv = mt["__name"];
        if (!nv.isString())
        {
            luaL_argerror(engine->internalState(), idx, "Argument does not have a valid name");
        }
        if (nv.toString() != "Serial")
        {
            luaL_argerror(engine->internalState(), idx, "Argument is not an Shell object");
        }
        Serial *ptr = reinterpret_cast<Serial *>(self.toUserData());
        t.reset();
        nv.destroy();
        mt.destroy();
        self.destroy();
        return ptr;
    }
    else if (a.isUserData())
    {
        auto mt = engine->getMetaTable(idx);
        auto nv = mt["__name"];
        if (!nv.isString())
        {
            luaL_argerror(engine->internalState(), idx, "Argument does not have a valid name");
        }
        if (nv.toString() != "Serial")
        {
            luaL_argerror(engine->internalState(), idx, "Argument is not an Shell object");
        }
        Serial *ptr = reinterpret_cast<Serial *>(a.toUserData());
        a.reset();
        nv.destroy();
        mt.destroy();
        return ptr;
    }
    else
    {
        luaL_argerror(engine->internalState(), idx, "argument must be user data");
        return nullptr;
    }
}

void Serial::removeCallbacks()
{    
    auto removeFunc = [&](int64_t id) {
        if(_engine.expired()) return;
        auto engine = _engine.lock();
        engine->removeReference(id);
    };

    std::for_each(_dataCallbacks.begin(), _dataCallbacks.end(), removeFunc);
}

int Serial::L_new(Engine *engine)
{

    if (engine->stackSize() < 1 || engine->stackSize() > 2)
    {
        luaL_error(engine->internalState(), "Invalid number of arguments");
    }

    SerialSettings settings;

    auto val = engine->getArgument(1);
    if(val.isTable()) {
        // Read parameters from table
        if(engine->stackSize() != 1) {
            luaL_error(engine->internalState(), "Invalid number of arguments");
        }
        auto t = engine->getArgument(1).toTable();
        settings.setDevice(QString::fromStdString(t["device"].toString()));
        settings.setDataBits(static_cast<QSerialPort::DataBits>(t["data"].toInteger()));
        settings.setStopBits(static_cast<QSerialPort::StopBits>(t["stop"].toInteger()));
        settings.setBaudrate(t["baudrate"].toInteger());
        if(!settings.setParity(QString::fromStdString(t["parity"].toString()))) {
            luaL_argerror(engine->internalState(), 1, "Invalid parity");
        }
        if(!settings.setFlowControl(QString::fromStdString(t["flowControl"].toString())))
        {
            luaL_argerror(engine->internalState(), 1, "Invalid flow control");
        }
    } else if(val.isString()) {
        // Read device name from table
        settings.setDevice(QString::fromStdString(engine->checkString(1)));
        if(engine->stackSize() >= 2) {
            settings.setBaudrate(engine->checkInteger(2));
        }
    }
    engine->pop(engine->stackSize());

    auto t = engine->createTable(0, 1);
    Serial *self = engine->allocUserData<Serial>("Serial", engine->shared_from_this(), settings);
    t.insert("self");
    t.insert("registerDataCallback", Serial::L_registerDataCallback);
    t.insert("write", Serial::L_write);
    t.insert("getSettings", Serial::L_getSettings);
    t.insert("setBaudRate", Serial::L_setBaudRate);
    t.insert("setDataBits", Serial::L_setDataBits);
    t.insert("setStopBits", Serial::L_setStopBits);
    t.insert("setParity", Serial::L_setParity);
    t.insert("setFlowControl", Serial::L_setFlowControl);

    auto mt = engine->createTable();
    mt.insert("__metatable", std::move(engine->createTable()));

    engine->setMetaTable(-2, std::move(mt));
    t.reset();
    return 1;
}

int Serial::L_write(Engine *engine)
{
    if(engine->stackSize() != 2) {
        luaL_error(engine->internalState(), "Invalid number of arguments");
    }
    auto *self = Serial::CheckObject(engine, 1);
    std::string str = engine->checkString(2);
    engine->pop(2);

    self->write(QByteArray(str.data(), str.size()));
    return 0;
}

int Serial::L_registerDataCallback(Engine *engine)
{
    if (engine->stackSize() != 2)
    {
        luaL_error(engine->internalState(), "Invalid number of arguments");
    }
    auto *self = Serial::CheckObject(engine, 1);
    auto value = engine->getArgument(2);
    if (!value.isFunction())
    {
        luaL_argerror(engine->internalState(), 2, "Must be a function");
    }
    int64_t id = engine->storeReference(std::move(value));
    engine->pop(1);
    self->_dataCallbacks.insert(id);
    return 0;
}

int Serial::L_getSettings(Engine *engine)
{
    if(engine->stackSize() != 1) {
        luaL_error(engine->internalState(), "Invalid number of arguments");
    }
    auto *self = Serial::CheckObject(engine, 1);
    engine->pop(1);

    auto settings = self->settings();

    auto t = engine->createTable(0, 6);
    t.insert("device", settings.device().toStdString());
    t.insert("parity", settings.parityString().toStdString());
    t.insert("baudrate", static_cast<int64_t>(settings.baudrate()));
    t.insert("data", settings.dataBits());
    t.insert("stop", settings.stopBits());
    t.insert("flowControl", settings.flowControlString().toStdString());
    t.reset();

    return 1;
}

int Serial::L_setBaudRate(Engine *engine)
{
    if(engine->stackSize() != 2) {
        luaL_error(engine->internalState(), "Invalid number of arguments");
    }
    auto *self = Serial::CheckObject(engine, 1);
    int64_t baudrate = engine->getArgument(2).toInteger();
    engine->pop(2);

    self->_serialPort->setBaudRate(baudrate);

    return 0;
}

int Serial::L_setDataBits(Engine *engine)
{
    if(engine->stackSize() != 2) {
        luaL_error(engine->internalState(), "Invalid number of arguments");
    }
    auto *self = Serial::CheckObject(engine, 1);
    int64_t bitsNum = engine->getArgument(2).toInteger();
    engine->pop(2);

    QSerialPort::DataBits bits = SerialSettings::intToDataBits(bitsNum);
    if(bits == QSerialPort::UnknownDataBits)
    {
        luaL_argerror(engine->internalState(), 2, "Invalid data bits");
    }

    self->_serialPort->setDataBits(bits);

    return 0;
}

int Serial::L_setStopBits(Engine *engine)
{
    if(engine->stackSize() != 2) {
        luaL_error(engine->internalState(), "Invalid number of arguments");
    }
    auto *self = Serial::CheckObject(engine, 1);
    int64_t bitsNum = engine->getArgument(2).toInteger();
    engine->pop(2);

    QSerialPort::StopBits bits = SerialSettings::intToStopBits(bitsNum);
    if(bits == QSerialPort::UnknownStopBits)
    {
        luaL_argerror(engine->internalState(), 2, "Invalid stop bits");
    }

    self->_serialPort->setStopBits(bits);

    return 0;
}

int Serial::L_setParity(Engine *engine)
{
    if(engine->stackSize() != 2) {
        luaL_error(engine->internalState(), "Invalid number of arguments");
    }
    auto *self = Serial::CheckObject(engine, 1);
    auto parityStr = engine->getArgument(2).toString();
    engine->pop(2);


    QSerialPort::Parity parity = SerialSettings::stringToParity(QString::fromStdString(parityStr));
    if(parity == QSerialPort::UnknownParity) {
        luaL_argerror(engine->internalState(), 2, "Invalid parity");
    }

    self->_serialPort->setParity(parity);

    return 0;
}

int Serial::L_setFlowControl(Engine *engine)
{

    if(engine->stackSize() != 2) {
        luaL_error(engine->internalState(), "Invalid number of arguments");
    }
    auto *self = Serial::CheckObject(engine, 1);
    std::string fcStr = engine->getArgument(2).toString();
    engine->pop(2);

    QSerialPort::FlowControl fc = SerialSettings::stringToFlowControl(QString::fromStdString(fcStr));
    if(fc == QSerialPort::UnknownFlowControl) {
        luaL_argerror(engine->internalState(), 2, "Invalid flow control");
    }

    self->_serialPort->setFlowControl(fc);

    return 0;
}
