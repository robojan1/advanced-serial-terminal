#ifndef ENGINE_VTE_H
#define ENGINE_VTE_H

#include <QObject>
#include <windows/vte.h>
#include <engine/engine.h>
#include <unordered_set>

namespace lua {

    class VTE : public QObject {
        Q_OBJECT
    public:
        VTE(std::weak_ptr<Engine> engine);
        ~VTE();
        static void registerLua(Engine *engine);

        void setTitle(const std::string &title);
    
    signals:

    public slots:
        void write(const std::string &data);

    private slots:
        void onWindowResized(QSize size);
        void onInputReceived(const QByteArray &data);
        void onWindowDestroyed(QObject *object);

    private:
        windows::VTE *_window;
        std::weak_ptr<Engine> _engine;
        std::unordered_set<int64_t> _onInputCallbacks;
        std::unordered_set<int64_t> _onResizeCallbacks;


        static int L_new(Engine *engine);
        static int L_write(Engine *engine);
        static int L_close(Engine *engine);
        static int L_registerInputCallback(Engine *engine);
        static int L_registerResizeCallback(Engine *engine);
        static VTE *checkValue(Engine *engine, int idx);
        
        void removeCallbacks();
    };
}

#endif
