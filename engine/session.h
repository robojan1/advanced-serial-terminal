#ifndef ENGINE_SESSION_H
#define ENGINE_SESSION_H

#include <unordered_set>
#include <memory>
#include <engine/engine.h>
#include <QObject>
#include <QDockWidget>

namespace windows {
    class BaseWindow;
}

class Session : public QObject
{
Q_OBJECT
    friend class std::hash<Session>;
public:
    Session(QObject *parent = nullptr);
    Session(Session &&other);
    Session(const Session &other) = delete;
    ~Session();

    bool hasActiveWindow() const { 
        return _activeWindows.size() > 0;
    }

    void run(const std::string &script);
    void runFile(const std::string &file);

    bool isDone() const;

    void setAddWindowCallback(std::function<void(windows::BaseWindow*)> addWindow);

signals:
    void done();

private:
    std::shared_ptr<lua::Engine> _engine;
    std::unordered_set<windows::BaseWindow *> _activeWindows;
    std::function<void(windows::BaseWindow*)> _addWindowCB;

    int L_addWindow(lua::Engine *engine);
    void setupEngine();

private slots:
    void onWindowClosing(windows::BaseWindow *window);
};

#endif