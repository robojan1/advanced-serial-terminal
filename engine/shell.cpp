#include <engine/shell.h>
#include <inputs/shell.h>

using namespace lua;

Shell::Shell(std::weak_ptr<Engine> engine, QString shell) : _engine(std::move(engine)), _shell(new inputs::Shell(this))
{
    connect(_shell, &inputs::Shell::readyRead, this, &Shell::onReadyRead);
    connect(_shell, &inputs::Shell::exited, this, &Shell::onExited);

    _shell->open(shell);
}

Shell::~Shell()
{
    removeCallbacks();
}

void Shell::registerLua(Engine *engine)
{
    auto t = engine->createTable();
    t.insert("new", Shell::L_new);
    t.insert("registerDataCallback", Shell::L_registerDataCallback);
    t.insert("registerExitCallback", Shell::L_registerExitCallback);
    t.insert("write", Shell::L_write);
    t.insert("resize", Shell::L_resize);

    engine->storeGlobal("shell", std::move(t));
}

void Shell::write(const std::string &data)
{
    _shell->write(data.data(), data.size());
}

void Shell::resize(QSize size)
{
    _shell->resize(size);
}

void Shell::onReadyRead()
{
    if (_engine.expired())
        return;
    auto data = _shell->readAll();
    auto engine = _engine.lock();
    for (auto refId : _onDataCallbacks)
    {
        auto f = engine->getReference(refId);
        engine->pushValue(std::string(data.constData(), data.size()));
        try
        {
            engine->callStackFunction(1, 0);
        }
        catch (lua::error &e)
        {
            qWarning() << "Error calling data callback function: " << e.what();
        }
    }
}

void Shell::onExited(bool normal, int status)
{
    if (_engine.expired())
        return;
    auto data = _shell->readAll();
    auto engine = _engine.lock();
    for (auto refId : _onExitedCallbacks)
    {
        auto f = engine->getReference(refId);
        engine->pushValue(normal);
        engine->pushValue(status);
        try
        {
            engine->callStackFunction(2, 0);
        }
        catch (lua::error &e)
        {
            qWarning() << "Error calling exit callback function: " << e.what();
        }
    }
}

int Shell::L_new(Engine *engine)
{
    if (engine->stackSize() > 1)
    {
        luaL_error(engine->internalState(), "Invalid number of arguments");
    }

    QString shellBin;
    if (engine->stackSize() == 1)
    {
        // Get the shell from the argument
        auto val = engine->getArgument(1);
        if (!val.isString())
        {
            luaL_argerror(engine->internalState(), 1, "Should be a string");
        }
        shellBin = QString::fromStdString(val.toString());
        val.reset();
    }
    else
    {
        // Get the default shell
        shellBin = inputs::Shell::defaultShell();
    }

    auto t = engine->createTable(0, 1);
    Shell *self = engine->allocUserData<Shell>("Shell", engine->shared_from_this(), shellBin);
    t.insert("self");
    t.insert("registerDataCallback", Shell::L_registerDataCallback);
    t.insert("registerExitCallback", Shell::L_registerExitCallback);
    t.insert("write", Shell::L_write);
    t.insert("resize", Shell::L_resize);

    auto mt = engine->createTable();
    mt.insert("__metatable", std::move(engine->createTable()));

    engine->setMetaTable(-2, std::move(mt));
    t.reset();
    return 1;
}

int Shell::L_registerDataCallback(Engine *engine)
{
    if (engine->stackSize() != 2)
    {
        luaL_error(engine->internalState(), "Invalid number of arguments");
    }
    Shell *self = Shell::CheckObject(engine, 1);
    auto value = engine->getArgument(2);
    if (!value.isFunction())
    {
        luaL_argerror(engine->internalState(), 2, "Must be a function");
    }
    int64_t id = engine->storeReference(std::move(value));
    engine->pop(1);
    self->_onDataCallbacks.insert(id);
    return 0;
}

int Shell::L_registerExitCallback(Engine *engine)
{
    if (engine->stackSize() != 2)
    {
        luaL_error(engine->internalState(), "Invalid number of arguments");
    }
    Shell *self = Shell::CheckObject(engine, 1);
    auto value = engine->getArgument(2);
    if (!value.isFunction())
    {
        luaL_argerror(engine->internalState(), 2, "Must be a function");
    }
    int64_t id = engine->storeReference(std::move(value));
    engine->pop(1);
    self->_onExitedCallbacks.insert(id);
    return 0;
}

Shell *Shell::CheckObject(Engine *engine, int idx)
{
    Value a(engine->shared_from_this(), idx);
    if (a.isTable())
    {
        Table t = a.toTable();
        auto self = t["self"];
        auto mt = engine->getMetaTable(-1);
        auto nv = mt["__name"];
        if (!nv.isString())
        {
            luaL_argerror(engine->internalState(), idx, "Argument does not have a valid name");
        }
        if (nv.toString() != "Shell")
        {
            luaL_argerror(engine->internalState(), idx, "Argument is not an Shell object");
        }
        Shell *ptr = reinterpret_cast<Shell *>(self.toUserData());
        t.reset();
        nv.destroy();
        mt.destroy();
        self.destroy();
        return ptr;
    }
    else if (a.isUserData())
    {
        auto mt = engine->getMetaTable(idx);
        auto nv = mt["__name"];
        if (!nv.isString())
        {
            luaL_argerror(engine->internalState(), idx, "Argument does not have a valid name");
        }
        if (nv.toString() != "Shell")
        {
            luaL_argerror(engine->internalState(), idx, "Argument is not an Shell object");
        }
        Shell *ptr = reinterpret_cast<Shell *>(a.toUserData());
        a.reset();
        nv.destroy();
        mt.destroy();
        return ptr;
    }
    else
    {
        luaL_argerror(engine->internalState(), idx, "argument must be user data");
        return nullptr;
    }
}


int Shell::L_write(Engine *engine)
{
    if(engine->stackSize() != 2) {
        luaL_error(engine->internalState(), "Invalid number of arguments");
    }
    Shell *self = Shell::CheckObject(engine, 1);
    std::string str = engine->checkString(2);
    engine->pop(2);

    self->write(str);
    return 0;
}

int Shell::L_resize(Engine *engine)
{
    if(engine->stackSize() != 3) {
        luaL_error(engine->internalState(), "Invalid number of arguments");
    }
    Shell *self = Shell::CheckObject(engine, 1);
    int w = engine->checkInteger(2);
    int h = engine->checkInteger(3);
    engine->pop(3);

    self->resize(QSize(w,h));
    return 0;
}

void Shell::removeCallbacks() {
    auto removeFunc = [&](int64_t id) {
        if(_engine.expired()) return;
        auto engine = _engine.lock();
        engine->removeReference(id);
    };

    std::for_each(_onDataCallbacks.begin(), _onDataCallbacks.end(), removeFunc);
    std::for_each(_onExitedCallbacks.begin(), _onExitedCallbacks.end(), removeFunc);
}

