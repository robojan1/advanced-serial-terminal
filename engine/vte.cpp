#include <engine/engine.h>
#include <engine/vte.h>

using namespace lua;

VTE::VTE(std::weak_ptr<Engine> engine) : 
    _engine(std::move(engine)), _window(new windows::VTE) 
{
    connect(_window, &windows::VTE::inputReceived, this, &VTE::onInputReceived);
    connect(_window, &windows::VTE::resized, this, &VTE::onWindowResized);
    printf("Creating VTE\n");
}

VTE::~VTE() {
    removeCallbacks();
    printf("destroying VTE\n");
}

void VTE::registerLua(Engine *engine) {
    auto t = engine->createTable();
    t.insert("new", VTE::L_new);
    t.insert("write", VTE::L_write);
    t.insert("registerInputCallback", VTE::L_registerInputCallback);
    t.insert("registerResizeCallback", VTE::L_registerResizeCallback);
    t.insert("close", VTE::L_close);

    engine->storeGlobal("vte", std::move(t));
}

int VTE::L_new(Engine *engine) {

    if(engine->stackSize() != 0) {
        luaL_error(engine->internalState(), "Invalid number of arguments");
    }

    auto t = engine->createTable(0, 3);
    VTE *self = engine->allocUserData<VTE>("VTE", engine->shared_from_this());
    t.insert("self");
    t.insert("write", VTE::L_write);
    t.insert("registerInputCallback", VTE::L_registerInputCallback);
    t.insert("registerResizeCallback", VTE::L_registerResizeCallback);
    t.insert("close", VTE::L_close);

    auto mt = engine->createTable();
    mt.insert("__metatable", std::move(engine->createTable()));
    // mt.insert("__gc", +[](Engine *engine) -> int {
    //     VTE *self = VTE::checkValue(engine, 1);
    //     engine->pop();
    //     return 0;
    // });
    mt.insert("__dock_window", +[](Engine *engine) -> int {
        VTE *self = VTE::checkValue(engine, 1);
        engine->pop();
        engine->pushValue(self->_window);
        return 1;
    });
    engine->setMetaTable(-2, std::move(mt));
    t.reset();
    return 1;
}

VTE *VTE::checkValue(Engine *engine, int idx)
{
    Value a(engine->shared_from_this(), idx);
    if(a.isTable()) {
        Table t = a.toTable();
        auto self = t["self"];
        auto mt = engine->getMetaTable(-1);
        auto nv = mt["__name"];
        if(!nv.isString()) {
            luaL_argerror(engine->internalState(), idx, "Argument does not have a valid name");
        }
        if(nv.toString() != "VTE") {
            luaL_argerror(engine->internalState(), idx, "Argument is not an VTE object");
        }
        VTE *ptr = reinterpret_cast<VTE *>(self.toUserData());
        t.reset();
        nv.destroy();
        mt.destroy();
        self.destroy();
        return ptr;
    } else if(a.isUserData()) {
        auto mt = engine->getMetaTable(idx);
        auto nv = mt["__name"];
        if(!nv.isString()) {
            luaL_argerror(engine->internalState(), idx, "Argument does not have a valid name");
        }
        if(nv.toString() != "VTE") {
            luaL_argerror(engine->internalState(), idx, "Argument is not an VTE object");
        }
        VTE *ptr = reinterpret_cast<VTE *>(a.toUserData());
        a.reset();
        nv.destroy();
        mt.destroy();
        return ptr;
    } else {
        luaL_argerror(engine->internalState(), idx, "argument must be user data");
        return nullptr;
    }
}

int VTE::L_write(Engine *engine)
{
    if(engine->stackSize() != 2) {
        luaL_error(engine->internalState(), "Invalid number of arguments");
    }
    VTE *self = VTE::checkValue(engine, 1);
    std::string str = engine->checkString(2);
    engine->pop(2);

    self->write(str);
    return 0;
}

int VTE::L_registerInputCallback(Engine *engine)
{
    if(engine->stackSize() != 2) {
        luaL_error(engine->internalState(), "Invalid number of arguments");
    }
    VTE *self = VTE::checkValue(engine, 1);
    auto value = engine->getArgument(2);
    if(!value.isFunction()) {
        luaL_argerror(engine->internalState(), 2, "Must be a function");
    }
    int64_t id = engine->storeReference(std::move(value));
    engine->pop(1);
    self->_onInputCallbacks.insert(id);
    return 0;
}

int VTE::L_registerResizeCallback(Engine *engine)
{
    if(engine->stackSize() != 2) {
        luaL_error(engine->internalState(), "Invalid number of arguments");
    }
    VTE *self = VTE::checkValue(engine, 1);
    auto value = engine->getArgument(2);
    if(!value.isFunction()) {
        luaL_argerror(engine->internalState(), 2, "Must be a function");
    }
    int64_t id = engine->storeReference(std::move(value));
    engine->pop(1);
    self->_onResizeCallbacks.insert(id);
    return 0;
}

void VTE::setTitle(const std::string &title) {
    _window->setWindowTitle(QString::fromStdString(title));
}

void VTE::write(const std::string &str)
{
    _window->write(QByteArray::fromStdString(str));
}

void VTE::onInputReceived(const QByteArray &data) {
    if(_engine.expired()) return;
    auto engine = _engine.lock();
    for(auto refId : _onInputCallbacks) {
        auto f = engine->getReference(refId);
        engine->pushValue(std::string(data.constData(), data.size()));
        try {
            engine->callStackFunction(1, 0);
        } catch(lua::error &e)
        {
            qWarning() << "Error calling data callback function: " << e.what();
        }
    }
}

void VTE::onWindowResized(QSize size) {
    if(_engine.expired()) return;
    auto engine = _engine.lock();
    for(auto refId : _onResizeCallbacks) {
        auto f = engine->getReference(refId);
        engine->pushValue(size.width());
        engine->pushValue(size.height());
        try {
            engine->callStackFunction(2, 0);
        } catch(lua::error &e)
        {
            qWarning() << "Error calling data callback function: " << e.what();
        }
    }
}

void VTE::onWindowDestroyed(QObject *object)
{
    
}

void VTE::removeCallbacks() {
    auto removeFunc = [&](int64_t id) {
        if(_engine.expired()) return;
        auto engine = _engine.lock();
        engine->removeReference(id);
    };

    std::for_each(_onInputCallbacks.begin(), _onInputCallbacks.end(), removeFunc);
    std::for_each(_onResizeCallbacks.begin(), _onResizeCallbacks.end(), removeFunc);
}

int VTE::L_close(Engine *engine)
{
    if(engine->stackSize() != 1) {
        luaL_error(engine->internalState(), "Invalid number of arguments");
    }
    VTE *self = VTE::checkValue(engine, 1);
    engine->pop(1);

    self->_window->close();

    return 0;
}
