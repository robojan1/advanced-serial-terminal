#ifndef ENGINE_ENGINE_H
#define ENGINE_ENGINE_H

struct luaState;

#include <lua.h>
#include "lualib.h"
#include "lauxlib.h"
#include <engine/error.h>
#include <functional>
#include <memory>
#include <cassert>

namespace lua {
    class Table;
    class Value;

    class Engine : public std::enable_shared_from_this<Engine>
    {
    public:
        static std::shared_ptr<Engine> make_engine(bool loadDefaultLibs = true);
        ~Engine();
        void destroy();

        void loadDefaultLibs();

        void doString(const char *str);
        void doFile(const char *file);

        void dumpStack() const;
        int stackSize() const;

        lua::Table createTable(int narr = 0, int nrec = 0);
        lua::Table getMetaTable(int idx = -1);
        void setMetaTable(int idx, lua::Table &&table);

        Value getArgument(int idx);
        Value getValue(int idx);
        std::string checkString(int idx);
        int64_t checkInteger(int idx);

        void pushValue(const char *value);
        void pushValue(const std::string &value);
        void pushValue(double value);
        void pushValue(int value);
        void pushValue(int64_t value);
        void pushValue(bool value);
        void pushValue(std::function<int(Engine *)> value, std::string name = "no-name");
        void pushValue(int (*value)(Engine *), const char *name = "no-name");
        void pushValue(Table &&value);
        void pushValue(Value &&value);
        void pushValue(const Table &value);
        void pushValue(const Value &value);
        void pushValue(void *value);
        void pushNil();

        void pop(int n = 1);

        template <typename T>
        void storeGlobal(const std::string &name, T value);
        void storeGlobal(const std::string &name, Table &&value);

        int64_t storeReference(Value &&value);
        void removeReference(int64_t id);
        Value getReference(int64_t  id);

        void callStackFunction(int nargs, int nresults);
        void protectedCallStackFunction(int nargs, int nresults);
        
        template<typename T, typename... Args>
        T* allocUserData(const std::string &name, Args... args);

        static Engine *getEngine(lua_State *L);

        lua_State *internalState() { return _state; }

    private:
        lua_State *_state;
        int64_t _funcIdCounter;
        int64_t _refCounter;
        struct UserFunction {
            std::function<int(Engine *)> callback;
            std::string name;
        };
        std::unordered_map<int64_t, UserFunction> _userFunctionMap;

        Engine(bool loadDefaultLibs);

        static void *allocator(void *ud, void *ptr, size_t osize, size_t nsize);
        static int errorHandler(lua_State *L);
        static int functionCallback(lua_State *L);

        void createEngineType();
        void createReferenceTabel();

        void check_lua_error(int err) {
            switch(err) {
            case LUA_ERRSYNTAX: throw lua::syntax_error(_state);
            case LUA_ERRRUN: throw lua::runtime_error(_state);
            case LUA_ERRERR: throw lua::error(_state);
            case LUA_ERRMEM: throw lua::memory_error(_state);
            case LUA_ERRGCMM: throw lua::garbage_collector_error(_state);
            default: break;
            }
        }
    };

    class Table {
    public:
        Table(std::shared_ptr<Engine> engine, int pos);
        Table(const Table &other) = delete;
        Table(Table &&other);
        ~Table();

        void destroy();
        void reset();

        template<typename T>
        Value operator[](T key);

        template <typename K, typename V>
        void insert(K key, V value);
        template <typename K>
        void insert(K key, Table &&value);
        template <typename K>
        void insert(K key);

        int stackIdx() const { return _stackIdx; }


    private:
        int _stackIdx;
        std::shared_ptr<Engine> _engine;
    };

    class Value
    {
    public:
        Value(std::shared_ptr<Engine> engine, int pos);
        Value(const Value &other) = delete;
        Value(Value &&other);
        ~Value();

        void destroy();
        void reset();

        bool isNil() const;
        bool isBoolean() const;
        bool isNumber() const;
        bool isString() const;
        bool isFunction() const;
        bool isCFunction() const;
        bool isUserData() const;
        bool isThread() const; 
        bool isTable() const;

        std::string toString() const;
        void *toUserData() const;
        Table toTable();
        int64_t toInteger() const;

        int stackIdx() const { return _stackIdx; }

    private:
        int _stackIdx;
        std::shared_ptr<Engine> _engine;
    };

    template<typename T, typename... Args>
    T* Engine::allocUserData(const std::string &name, Args... args)
    {
        T* ptr = reinterpret_cast<T*>(lua_newuserdata(_state, sizeof(T)));
        new(ptr) T(std::forward<Args>(args)...);

        lua::Table mt = createTable();
        mt.insert("__gc", +[](Engine *engine) -> int {
            lua_assert(lua_gettop() >= 1);
            T *obj = reinterpret_cast<T*>(lua_touserdata(engine->internalState(), 1));
            obj->~T();
            engine->pop();
            return 0;
        });
        mt.insert("__name", name);
        mt.reset();
        lua_setmetatable(_state, -2);
        return ptr;
    }

    template<typename T>
    void Engine::storeGlobal(const std::string &name, T value)
    {
        pushValue(std::forward<T>(value), name);
        lua_setglobal(_state, name.c_str());
    }

    template <typename K, typename V>
    void Table::insert(K key, V value) {
        _engine->pushValue(std::forward<K>(key));
        _engine->pushValue(std::forward<V>(value));
        lua_settable(_engine->internalState(), _stackIdx);
    }

    template <typename K>
    void Table::insert(K key, Table &&value) {
        assert(lua_gettop(_engine->internalState()) == value.stackIdx());
        value.reset();
        _engine->pushValue(std::forward<K>(key));
        lua_insert(_engine->internalState(), -2);
        lua_settable(_engine->internalState(), _stackIdx);
    }

    template <typename K>
    void Table::insert(K key) {
        _engine->pushValue(std::forward<K>(key));
        lua_insert(_engine->internalState(), -2);
        lua_settable(_engine->internalState(), _stackIdx);
    }

    template<typename T>
    Value Table::operator[](T key)
    {
        _engine->pushValue(std::forward<T>(key));
        lua_gettable(_engine->internalState(), _stackIdx);

        int pos = lua_absindex(_engine->internalState(), -1);
        return Value(_engine, pos);
    }

}



#endif // ENGINE_ENGINE_H
