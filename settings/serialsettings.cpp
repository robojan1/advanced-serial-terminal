#include "serialsettings.h"

QString SerialSettings::serialParityToString(QSerialPort::Parity parity)
{
    switch(parity)
    {
    case QSerialPort::NoParity:
        return QStringLiteral("none");
    case QSerialPort::EvenParity:
        return QStringLiteral("even");
    case QSerialPort::OddParity:
        return QStringLiteral("odd");
    case QSerialPort::SpaceParity:
        return QStringLiteral("space");
    case QSerialPort::MarkParity:
        return QStringLiteral("mark");
    default:
    case QSerialPort::UnknownParity:
        return QStringLiteral("unknown");

    }
}

QString SerialSettings::serialFlowControlToString(QSerialPort::FlowControl fc)
{
    switch(fc)
    {
    case QSerialPort::NoFlowControl:
        return QStringLiteral("none");
    case QSerialPort::HardwareControl:
        return QStringLiteral("hardware");
    case QSerialPort::SoftwareControl:
        return QStringLiteral("software");
    default:
    case QSerialPort::UnknownFlowControl:
        return QStringLiteral("unknown");

    }
}

bool SerialSettings::setFlowControl(QString flowControlString)
{
    QSerialPort::FlowControl fc = stringToFlowControl(flowControlString);
    if(fc == QSerialPort::UnknownFlowControl) {
        return false;
    }
    setFlowControl(fc);
    return true;
}

QString SerialSettings::flowControlString() const
{
    return serialFlowControlToString(flowControl());
}

bool SerialSettings::setParity(QString parityString)
{
    QSerialPort::Parity parity = stringToParity(parityString);
    if(parity == QSerialPort::UnknownParity)
    {
        return false;
    }
    setParity(parity);
    return true;
}

QString SerialSettings::parityString() const
{
    return serialParityToString(parity());
}


QSerialPort::FlowControl SerialSettings::stringToFlowControl(QString str)
{

    if(str.compare("none", Qt::CaseInsensitive) == 0) {
        return QSerialPort::NoFlowControl;
    } else if(str.compare("hardware", Qt::CaseInsensitive) == 0) {
        return QSerialPort::HardwareControl;
    } else if(str.compare("software", Qt::CaseInsensitive) == 0) {
        return QSerialPort::SoftwareControl;
    } else {
        return QSerialPort::UnknownFlowControl;
    }
}

QSerialPort::Parity SerialSettings::stringToParity(QString str)
{
    if(str.compare("none", Qt::CaseInsensitive) == 0) {
        return QSerialPort::NoParity;
    } else if(str.compare("even", Qt::CaseInsensitive) == 0) {
        return QSerialPort::EvenParity;
    } else if(str.compare("odd", Qt::CaseInsensitive) == 0) {
        return QSerialPort::OddParity;
    } else if(str.compare("mark", Qt::CaseInsensitive) == 0) {
        return QSerialPort::MarkParity;
    } else if(str.compare("space", Qt::CaseInsensitive) == 0) {
        return QSerialPort::SpaceParity;
    } else {
        return QSerialPort::UnknownParity;
    }
}


QSerialPort::DataBits SerialSettings::intToDataBits(int bits)
{
    switch(bits)
    {
        case QSerialPort::Data5:
        case QSerialPort::Data6:
        case QSerialPort::Data7:
        case QSerialPort::Data8:
            return static_cast<QSerialPort::DataBits>(bits);
        default:
            return QSerialPort::UnknownDataBits;
    }
}

QSerialPort::StopBits SerialSettings::intToStopBits(int bits)
{
    switch(bits)
    {
        case QSerialPort::OneStop:
        case QSerialPort::OneAndHalfStop:
        case QSerialPort::TwoStop:
            return static_cast<QSerialPort::StopBits>(bits);
        default:
            return QSerialPort::UnknownStopBits;
    }
}