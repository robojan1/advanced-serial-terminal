#ifndef SERIALSETTINGS_H
#define SERIALSETTINGS_H

#include <QObject>
#include <QSerialPort>

class SerialSettings
{
    Q_GADGET
    Q_PROPERTY(QString device READ device WRITE setDevice)
    Q_PROPERTY(QSerialPort::DataBits dataBits READ dataBits WRITE setDataBits)
    Q_PROPERTY(QSerialPort::StopBits stopBits READ stopBits WRITE setStopBits)
    Q_PROPERTY(QSerialPort::Parity parity READ parity WRITE setParity)
    Q_PROPERTY(QSerialPort::FlowControl flowControl READ flowControl WRITE setFlowControl)
    Q_PROPERTY(quint32 baudrate READ baudrate WRITE setBaudrate)
public:
    explicit SerialSettings() : _dataBits(QSerialPort::Data8), _stopBits(QSerialPort::OneStop),
        _parity(QSerialPort::NoParity), _flowControl(QSerialPort::NoFlowControl), _baudrate(9600) {}

    void setDevice(QString device) { _device = std::move(device); }
    const QString &device() const { return _device; }
    void setDataBits(QSerialPort::DataBits dataBits) { _dataBits = dataBits; }
    QSerialPort::DataBits dataBits() const { return _dataBits; }
    void setStopBits(QSerialPort::StopBits stopBits) { _stopBits = stopBits; }
    QSerialPort::StopBits stopBits() const { return _stopBits; }
    void setParity(QSerialPort::Parity parity) { _parity = parity; }
    bool setParity(QString parityString);
    QSerialPort::Parity parity() const { return _parity; }
    QString parityString() const;
    void setFlowControl(QSerialPort::FlowControl flowControl) { _flowControl = flowControl; }
    bool setFlowControl(QString flowControlString);
    QSerialPort::FlowControl flowControl() const { return _flowControl; }
    QString flowControlString() const;
    void setBaudrate(quint32 baudrate) { _baudrate = baudrate; }
    quint32 baudrate() const { return _baudrate; }

    static QString serialParityToString(QSerialPort::Parity parity);
    static QString serialFlowControlToString(QSerialPort::FlowControl fc);
    static QSerialPort::FlowControl stringToFlowControl(QString string);
    static QSerialPort::Parity stringToParity(QString string);
    static QSerialPort::DataBits intToDataBits(int bits);
    static QSerialPort::StopBits intToStopBits(int bits);

signals:

public slots:

private:
    QString _device;
    QSerialPort::DataBits _dataBits;
    QSerialPort::StopBits _stopBits;
    QSerialPort::Parity _parity;
    QSerialPort::FlowControl _flowControl;
    quint32 _baudrate;

};

#endif // SERIALSETTINGS_H
