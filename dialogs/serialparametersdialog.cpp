#include "serialparametersdialog.h"
#include "ui_serialparametersdialog.h"
#include <QSerialPortInfo>
#include <QSerialPort>
#include <QAbstractButton>

SerialParametersDialog::SerialParametersDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SerialParametersDialog)
{
    ui->setupUi(this);
    connect(ui->dlgButtons, &QDialogButtonBox::accepted, this, &SerialParametersDialog::accept);
    connect(ui->dlgButtons, &QDialogButtonBox::rejected, this, &SerialParametersDialog::reject);
    connect(ui->dlgButtons, &QDialogButtonBox::clicked, this, &SerialParametersDialog::onDialogButtonClicked);
    initializeElements();
}

SerialParametersDialog::~SerialParametersDialog()
{
    delete ui;
}

SerialSettings SerialParametersDialog::settings() const
{
    SerialSettings settings;
    settings.setDevice(ui->deviceComboBox->currentText());
    settings.setBaudrate(ui->baudrateComboBox->currentText().toUInt());
    settings.setParity(ui->parityComboBox->currentData().value<QSerialPort::Parity>());
    settings.setDataBits(ui->dataBitsComboBox->currentData().value<QSerialPort::DataBits>());
    settings.setStopBits(ui->stopBitsComboBox->currentData().value<QSerialPort::StopBits>());
    settings.setFlowControl(ui->flowControlComboBox->currentData().value<QSerialPort::FlowControl>());

    return settings;
}

void SerialParametersDialog::setDefaultSettings(SerialSettings settings)
{
    _defaultSettings = std::move(settings);
}

void SerialParametersDialog::setSettings(SerialSettings settings)
{
    ui->deviceComboBox->setCurrentText(settings.device());
    ui->baudrateComboBox->setCurrentText(QString::number(settings.baudrate()));
    int index = ui->parityComboBox->findData(settings.parity());
    if(index >= 0) ui->parityComboBox->setCurrentIndex(index);
    index = ui->dataBitsComboBox->findData(settings.dataBits());
    if(index >= 0) ui->dataBitsComboBox->setCurrentIndex(index);
    index = ui->stopBitsComboBox->findData(settings.stopBits());
    if(index >= 0) ui->stopBitsComboBox->setCurrentIndex(index);
    index = ui->flowControlComboBox->findData(settings.stopBits());
    if(index >= 0) ui->flowControlComboBox->setCurrentIndex(index);
}

void SerialParametersDialog::onDialogButtonClicked(QAbstractButton *button)
{
    if(ui->dlgButtons->standardButton(button) == QDialogButtonBox::RestoreDefaults) {
        onRestoreDefaultsClicked();
    }
}

void SerialParametersDialog::initializeElements()
{
    auto ports = QSerialPortInfo::availablePorts();
    auto baudrates = QSerialPortInfo::standardBaudRates();

    // Setting devices
    ui->deviceComboBox->clear();
    for(auto &port : ports) {
        ui->deviceComboBox->addItem(port.portName(), port.systemLocation());
    }

    // Setting baudrates
    ui->baudrateComboBox->clear();
    for(auto baudrate : baudrates) {
        ui->baudrateComboBox->addItem(QString::number(baudrate), baudrate);
    }

    // Setting the parity
    ui->parityComboBox->clear();
    ui->parityComboBox->addItem(tr("None"), QSerialPort::NoParity);
    ui->parityComboBox->addItem(tr("Even"), QSerialPort::EvenParity);
    ui->parityComboBox->addItem(tr("Odd"), QSerialPort::OddParity);
    ui->parityComboBox->addItem(tr("Space"), QSerialPort::SpaceParity);
    ui->parityComboBox->addItem(tr("Mark"), QSerialPort::MarkParity);

    // Setting the data bits
    ui->dataBitsComboBox->clear();
    ui->dataBitsComboBox->addItem(QString::number(5), QSerialPort::Data5);
    ui->dataBitsComboBox->addItem(QString::number(6), QSerialPort::Data6);
    ui->dataBitsComboBox->addItem(QString::number(7), QSerialPort::Data7);
    ui->dataBitsComboBox->addItem(QString::number(8), QSerialPort::Data8);

    // setting the stop bits
    ui->stopBitsComboBox->clear();
    ui->stopBitsComboBox->addItem(QString::number(1), QSerialPort::OneStop);
    ui->stopBitsComboBox->addItem(QString::number(1.5), QSerialPort::OneAndHalfStop);
    ui->stopBitsComboBox->addItem(QString::number(2), QSerialPort::TwoStop);

    // setting the flow control
    ui->flowControlComboBox->clear();
    ui->flowControlComboBox->addItem(tr("Off"), QSerialPort::NoFlowControl);
    ui->flowControlComboBox->addItem(tr("Software"), QSerialPort::SoftwareControl);
    ui->flowControlComboBox->addItem(tr("Hardware"), QSerialPort::HardwareControl);

    setSettings(defaultSettings());
}

void SerialParametersDialog::onRestoreDefaultsClicked()
{
    setSettings(_defaultSettings);
}
