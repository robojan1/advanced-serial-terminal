#ifndef SERIALPARAMETERSDIALOG_H
#define SERIALPARAMETERSDIALOG_H

#include <QDialog>

#include <settings/serialsettings.h>

class QAbstractButton;

namespace Ui {
class SerialParametersDialog;
}

class SerialParametersDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SerialParametersDialog(QWidget *parent = nullptr);
    ~SerialParametersDialog();

    SerialSettings settings() const;
    const SerialSettings &defaultSettings() const { return _defaultSettings; }
    void setDefaultSettings(SerialSettings settings);
    void setSettings(SerialSettings settings);

private slots:
    void onDialogButtonClicked(QAbstractButton *button);

private:
    Ui::SerialParametersDialog *ui;
    SerialSettings _defaultSettings;

    void initializeElements();

    void onRestoreDefaultsClicked();
};

#endif // SERIALPARAMETERSDIALOG_H
