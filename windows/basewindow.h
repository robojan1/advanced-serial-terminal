#ifndef WINDOWS_BASEWINDOW_H
#define WINDOWS_BASEWINDOW_H

#include <QDockWidget>
#include <windows/basewindowtypes.h>
#include <QDebug>

namespace windows {

    class BaseWindow : public QDockWidget
    {
        Q_OBJECT
    public:
        using QDockWidget::QDockWidget;
        virtual ~BaseWindow() = default;
        virtual BaseWindowTypes windowType() const = 0;

    signals:
        void windowClosing(BaseWindow *window);

    protected:
        void closeEvent(QCloseEvent *event) override {
            emit windowClosing(this);
            QDockWidget::closeEvent(event);
        }
    };

}

#endif // WINDOWS_BASEWINDOW_H
