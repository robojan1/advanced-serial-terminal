#include <windows/vte.h>

using namespace windows;

VTE::VTE(QWidget *parent) :
    BaseWindow (parent), _term(nullptr)
{
    createInstance();
}

void VTE::createInstance()
{
    setWindowTitle("Terminal");

    _term = new terminal::Terminal(this);
    _term->setMaxScrollBack(1000);
    setWidget(_term);

    connect(_term, &terminal::Terminal::inputReceived, this, &VTE::inputReceived);
    connect(_term, &terminal::Terminal::terminalResized, this, &VTE::resized);
    connect(_term, &terminal::Terminal::titleChanged, this, &VTE::onTitleChanged);
}

void VTE::closeEvent(QCloseEvent *event)
{
    disconnect(_term, &terminal::Terminal::inputReceived, this, &VTE::inputReceived);
    disconnect(_term, &terminal::Terminal::terminalResized, this, &VTE::resized);
    disconnect(_term, &terminal::Terminal::titleChanged, this, &VTE::onTitleChanged);
    BaseWindow::closeEvent(event);
}

void VTE::onTitleChanged()
{
    setWindowTitle(_term->title());
}

void VTE::write(const QByteArray &data)
{
    _term->writeOutput(data);
}