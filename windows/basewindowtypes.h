#ifndef WINDOWS_BASEWINDOWTYPES_H
#define WINDOWS_BASEWINDOWTYPES_H

namespace windows {

    enum class BaseWindowTypes {
        TerminalWindow,
        CodeEditorWindow
    };
}

#endif // WINDOWS_BASEWINDOWTYPES_H
