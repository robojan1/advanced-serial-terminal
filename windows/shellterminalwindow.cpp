#include <windows/shellterminalwindow.h>

using namespace windows;

ShellTerminalWindow::ShellTerminalWindow(const QString &shell, QWidget *parent) :
    BaseWindow (parent), _term(nullptr), _shell(nullptr)
{
    createInstance(shell);
}

void ShellTerminalWindow::createInstance(const QString &executable)
{
    setWindowTitle("Terminal");

    _term = new terminal::Terminal(this);
    _term->setMaxScrollBack(1000);
    setWidget(_term);

    _shell = new inputs::Shell(this);
    connect(_shell, &inputs::Shell::readyRead, this, &ShellTerminalWindow::onShellReadyRead);
    connect(_term, &terminal::Terminal::inputReceived, this, &ShellTerminalWindow::onTerminalData);
    connect(_term, &terminal::Terminal::terminalResized, _shell, &inputs::Shell::resize);
    connect(_term, &terminal::Terminal::titleChanged, this, &ShellTerminalWindow::onTitleChanged);

    _shell->open(executable);
}

void ShellTerminalWindow::closeEvent(QCloseEvent *event)
{
    disconnect(_shell, &inputs::Shell::readyRead, this, &ShellTerminalWindow::onShellReadyRead);
    disconnect(_term, &terminal::Terminal::inputReceived, this, &ShellTerminalWindow::onTerminalData);
    disconnect(_term, &terminal::Terminal::terminalResized, _shell, &inputs::Shell::resize);
    disconnect(_term, &terminal::Terminal::titleChanged, this, &ShellTerminalWindow::onTitleChanged);
    BaseWindow::closeEvent(event);
}

void ShellTerminalWindow::onShellReadyRead()
{
    _term->writeOutput(_shell->readAll());
}

void ShellTerminalWindow::onTerminalData(const QByteArray &data)
{
    _shell->write(data);
}

void ShellTerminalWindow::onTitleChanged()
{
    setWindowTitle(_term->title());
}
