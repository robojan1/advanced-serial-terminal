#ifndef WINDOWS_VTE_H
#define WINDOWS_VTE_H

#include <windows/basewindow.h>

#include <terminal/terminal.h>
#include <inputs/shell.h>


namespace windows {
    class VTE : public BaseWindow
    {
        Q_OBJECT
    public:
        VTE(QWidget *parent = nullptr);

        BaseWindowTypes windowType() const override { return BaseWindowTypes::TerminalWindow; }

        QByteArray read(int maxSize = -1);

    protected:
        using QDockWidget::setWidget;
        void closeEvent(QCloseEvent *event) override;

    signals:
        void inputReceived(const QByteArray &data);
        void resized(QSize size);

    public slots:
        void write(const QByteArray &data);

    private slots:
        void onTitleChanged();

    private:
        terminal::Terminal *_term;

        void createInstance();
    };
}

#endif // WINDOWS_SHELLTERMINALWINDOW_H
