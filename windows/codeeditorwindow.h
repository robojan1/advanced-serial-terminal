#ifndef WINDOWS_CODEEDITORWINDOW_H
#define WINDOWS_CODEEDITORWINDOW_H

#include <windows/basewindow.h>

class QCodeEditor;

namespace windows {

    class CodeEditorWindow : public BaseWindow
    {
        Q_OBJECT
    public:
        CodeEditorWindow(QWidget *parent = nullptr);    
        BaseWindowTypes windowType() const override { return BaseWindowTypes::CodeEditorWindow; }

        QString text() const;

    private:
        QCodeEditor *_editor;

        void createInstance();
    };

}

#endif // WINDOWS_CODEEDITORWINDOW_H
