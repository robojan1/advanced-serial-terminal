#include <windows/codeeditorwindow.h>
#include <QCodeEditor>

using namespace windows;

CodeEditorWindow::CodeEditorWindow(QWidget *parent) :
    BaseWindow(parent), _editor(nullptr)
{
    createInstance();
}

QString CodeEditorWindow::text() const
{
    return _editor->toPlainText();
}

void CodeEditorWindow::createInstance()
{
    _editor = new QCodeEditor(this);
    setWidget(_editor);
}
