#ifndef WINDOWS_SHELLTERMINALWINDOW_H
#define WINDOWS_SHELLTERMINALWINDOW_H

#include <windows/basewindow.h>

#include <terminal/terminal.h>
#include <inputs/shell.h>

namespace windows {

class ShellTerminalWindow : public BaseWindow
{
    Q_OBJECT
public:
    ShellTerminalWindow(const QString &shell, QWidget *parent = nullptr);

    BaseWindowTypes windowType() const override { return BaseWindowTypes::TerminalWindow; }

protected:
    using QDockWidget::setWidget;
    void closeEvent(QCloseEvent *event) override;

private slots:
    void onShellReadyRead();
    void onTerminalData(const QByteArray &data);
    void onTitleChanged();

private:
    terminal::Terminal *_term;
    inputs::Shell *_shell;

    void createInstance(const QString &executable);
};

}

#endif // WINDOWS_SHELLTERMINALWINDOW_H
