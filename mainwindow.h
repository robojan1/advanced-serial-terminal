#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <terminal/terminal.h>
#include <unordered_set>
#include <windows/basewindow.h>
#include <engine/session.h>

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);
    virtual ~MainWindow();

private:
    std::unordered_set<windows::BaseWindow *> _windows;
    std::unordered_set<Session *> _sessions;
    windows::BaseWindow *_activeWindow;
    QAction *_debugRunAction;

    void initialize();
    void createMenus();
    void createFileMenu();
    void addLuaFileToMenu(QMenu *menu, const QString &path);
    bool populateMenuFromDirectory(QMenu *menu, const QString &path);
    void populateNewMenu(QMenu *menu);
    void createStatusBar();
    void createDockableWindow();
    void createCodeEditor();
    void createDebugMenu();

    void addWindow(windows::BaseWindow *window);
    void updateDebugMenu();
    void cleanupSessions();
    void runScript(const QString &script);

private slots:
    void onSubWindowClosing(windows::BaseWindow *window);
    void onFocusChanged(QWidget *oldWidget, QWidget *newWidget);
};

#endif // MAINWINDOW_H
