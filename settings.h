#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>

class Settings : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString shell READ shell WRITE setShell NOTIFY shellChanged)
    Q_PROPERTY(QString systemScriptsDir READ systemScriptsDir)
public:
    ~Settings() = default;
    explicit Settings(const Settings &other) = delete;
    explicit Settings(Settings &&other) = delete;

    static Settings &Get();

    const QString &shell() const { return _shell; }
    void setShell(QString shell);

    QString systemScriptsDir() const;

signals:
    void shellChanged();

private:
    explicit Settings();

    QString _shell;
};

#endif // SETTINGS_H
