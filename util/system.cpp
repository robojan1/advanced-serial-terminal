#include <util/system.h>
#include <QFontDatabase>
#include <QDebug>

QFont getFixedFont()
{
    static QFont fixedFont = QFontDatabase::systemFont(QFontDatabase::FixedFont);
    if(!fixedFont.fixedPitch()) {
        QFontDatabase db;
        if(db.hasFamily("Hack")) {
            fixedFont = std::move(db.font("Hack", "Regular", 11));
        }
    }
    return fixedFont;
}