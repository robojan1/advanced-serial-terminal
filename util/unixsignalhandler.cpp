#include <util/unixsignalhandler.h>

#include <QMetaMethod>
#include <unistd.h>
#include <sys/signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <QDebug>

UnixSignalHandler::UnixSignalHandler() : QObject(nullptr), _notifier(nullptr)
{
    for(size_t i = 0; i < _signalInfo.size(); i++) {
        _signalInfo[i].refCount = 0;
    }

    int err = ::socketpair(AF_UNIX, SOCK_STREAM, 0, _socket.data());
    Q_ASSERT(err == 0);
    _notifier = new QSocketNotifier(_socket[1], QSocketNotifier::Read, this);
    connect(_notifier, &QSocketNotifier::activated, this, &UnixSignalHandler::onSocketData);
}

UnixSignalHandler::~UnixSignalHandler()
{
    close(_socket[0]);
    close(_socket[1]);
    for(size_t i = 0; i < _signalInfo.size(); i++) {
        if(_signalInfo[i].refCount > 0) {
            doDetachSignal(static_cast<int>(i));
        }
    }
}

UnixSignalHandler *UnixSignalHandler::get()
{
    static UnixSignalHandler *self = new UnixSignalHandler();
    return self;
}

void UnixSignalHandler::enableSignal(int signal)
{
    if(signal < 0 || signal >= _NSIG)
        throw std::range_error("Invalid signal");

    if(_signalInfo[static_cast<size_t>(signal)].refCount == 0) {
        // Attach the signal
        doAttachSignal(signal);
    }

    _signalInfo[static_cast<size_t>(signal)].refCount++;
}

void UnixSignalHandler::disableSignal(int signal)
{
    Q_ASSERT(_signalInfo[static_cast<size_t>(signal)].refCount > 0);
    _signalInfo[static_cast<size_t>(signal)].refCount--;

    if(_signalInfo[static_cast<size_t>(signal)].refCount == 0) {
        // Detach the signal
        doDetachSignal(signal);
    }
}

void UnixSignalHandler::connectNotify(const QMetaMethod &signal)
{
    auto id = signalFromMethod(signal);
    if(id < 0) return;
    enableSignal(id);
}

void UnixSignalHandler::disconnectNotify(const QMetaMethod &signal)
{
    auto id = signalFromMethod(signal);
    if(id < 0) return;
    disableSignal(id);
}

int UnixSignalHandler::signalFromMethod(const QMetaMethod &signal)
{
    if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalIO)) {
        return SIGIO;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalBus)) {
        return SIGBUS;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalPower)) {
        return SIGPWR;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalPipe)) {
        return SIGPIPE;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalQuit)) {
        return SIGQUIT;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalStop)) {
        return SIGSTOP;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalAbort)) {
        return SIGABRT;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalAlarm)) {
        return SIGALRM;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalChild)) {
        return SIGCHLD;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalUser1)) {
        return SIGUSR1;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalUser2)) {
        return SIGUSR2;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalHangup)) {
        return SIGHUP;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalUrgent)) {
        return SIGURG;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalContinue)) {
        return SIGCONT;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalInterrupt)) {
        return SIGINT;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalTerminate)) {
        return SIGTERM;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalTypedStop)) {
        return SIGTSTP;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalBadSyscall)) {
        return SIGSYS;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalBreakpoint)) {
        return SIGTRAP;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalVirtualAlarm)) {
        return SIGVTALRM;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalWindowResize)) {
        return SIGWINCH;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalTerminalInput)) {
        return SIGTTIN;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalTerminalOutput)) {
        return SIGTTOU;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalSegmentationFault)) {
        return SIGSEGV;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalIllegalInstruction)) {
        return SIGILL;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalCpuTimeLimitExceeded)) {
        return SIGXCPU;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalFileSizeLimitExceeded)) {
        return SIGXFSZ;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalProfilingTimerExpired)) {
        return SIGPROF;
    } else if(signal == QMetaMethod::fromSignal(&UnixSignalHandler::signalFloatingPointException)) {
        return SIGFPE;
    } else {
        return -1;
    }
}

void UnixSignalHandler::doAttachSignal(int signal)
{
    Q_ASSERT(signal >= 0 && signal < _NSIG);

    int err;

    struct sigaction action = {};
    action.sa_sigaction = UnixSignalHandler::signalHandler;
    sigemptyset(&action.sa_mask);
    action.sa_flags = SA_SIGINFO;

    err = sigaction(signal, &action, nullptr);
    Q_ASSERT(err == 0);
}

void UnixSignalHandler::doDetachSignal(int signal)
{
    Q_ASSERT(signal >= 0 && signal < _NSIG);

    int err;
    struct sigaction action = {};
    sigemptyset(&action.sa_mask);
    action.sa_handler = SIG_DFL;
    action.sa_flags = 0;

    err = sigaction(signal, &action, nullptr);
    if(err < 0) {
        throw std::runtime_error("Could not install signal handler");
    }
}

void UnixSignalHandler::signalHandler(int signal, siginfo_t *info, void *ucontext)
{
    Q_UNUSED(ucontext);
    auto self = UnixSignalHandler::get();

    // Write data to the socket. The handler will not be notified until the signal handler is finished.
    // So it is safe to do two writes
    ::write(self->_socket[0], &signal, sizeof(int));
    ::write(self->_socket[0], info, sizeof(siginfo_t));
}

void UnixSignalHandler::onSocketData()
{
    int signal;
    siginfo_t info;

    // Get information about the signal
    ::read(_socket[1], &signal, sizeof(int));
    ::read(_socket[1], &info, sizeof(siginfo_t));

    qDebug() << "Signal " << signal << " caught";

    this->signal(signal, info.si_code, info.si_pid, info.si_uid);
    switch(signal) {
    case SIGIO:
        signalIO(info.si_fd, info.si_band, info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGBUS:
        signalBus(info.si_addr, info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGPWR:
        signalPower(info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGPIPE:
        signalPipe(info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGQUIT:
        signalQuit(info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGSTOP:
        signalStop(info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGABRT:
        signalAbort(info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGALRM:
        signalAlarm(info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGCHLD:
        signalChild(info.si_status, info.si_pid, info.si_uid, info.si_code, info.si_utime, info.si_stime);
        break;
    case SIGUSR1:
        signalUser1(info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGUSR2:
        signalUser2(info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGHUP:
        signalHangup(info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGURG:
        signalUrgent(info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGCONT:
        signalContinue(info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGINT:
        signalInterrupt(info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGTERM:
        signalTerminate(info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGTSTP:
        signalTypedStop(info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGSYS:
        signalBadSyscall(info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGTRAP:
        signalBreakpoint(info.si_addr, info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGVTALRM:
        signalVirtualAlarm(info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGWINCH:
        signalWindowResize(info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGTTIN:
        signalTerminalInput(info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGTTOU:
        signalTerminalOutput(info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGSEGV:
        signalSegmentationFault(info.si_addr, info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGILL:
        signalIllegalInstruction(info.si_addr, info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGXCPU:
        signalCpuTimeLimitExceeded(info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGXFSZ:
        signalFileSizeLimitExceeded(info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGPROF:
        signalProfilingTimerExpired(info.si_code, info.si_pid, info.si_uid);
        break;
    case SIGFPE:
        signalFloatingPointException(info.si_addr, info.si_code, info.si_pid, info.si_uid);
        break;
    default: break;
    }
}
