#ifndef UTIL_UNIXSIGNALHANDLER_H
#define UTIL_UNIXSIGNALHANDLER_H

#include <QObject>
#include <QSocketNotifier>
#include <sys/signal.h>

class UnixSignalHandler : public QObject
{
    Q_OBJECT
public:
    enum Action {
        Terminate,
        Ignore,
        CoreDump,
        Stop,
        Continue
    };

    UnixSignalHandler(UnixSignalHandler &&) = delete;
    UnixSignalHandler(const UnixSignalHandler &) = delete;
    virtual ~UnixSignalHandler();

    static UnixSignalHandler *get();

    void enableSignal(int signal);
    void disableSignal(int signal);

signals:
    void signal(int signal, int code, pid_t pid, uid_t uid);
    void signalAbort(int code, pid_t pid, uid_t uid);
    void signalAlarm(int code, pid_t pid, uid_t uid);
    void signalBus(void *addr, int code, pid_t pid, uid_t uid);
    void signalChild(int status, pid_t pid, uid_t uid, int code, clock_t utime, clock_t stime);
    void signalContinue(int code, pid_t pid, uid_t uid);
    void signalFloatingPointException(void *addr, int code, pid_t pid, uid_t uid);
    void signalHangup(int code, pid_t pid, uid_t uid);
    void signalIllegalInstruction(void *addr, int code, pid_t pid, uid_t uid);
    void signalPower(int code, pid_t pid, uid_t uid);
    void signalInterrupt(int code, pid_t pid, uid_t uid);
    void signalIO(int fd, long band, int code, pid_t pid, uid_t uid);
    void signalPipe(int code, pid_t pid, uid_t uid);
    void signalProfilingTimerExpired(int code, pid_t pid, uid_t uid);
    void signalQuit(int code, pid_t pid, uid_t uid);
    void signalSegmentationFault(void *addr, int code, pid_t pid, uid_t uid);
    void signalStop(int code, pid_t pid, uid_t uid);
    void signalTypedStop(int code, pid_t pid, uid_t uid);
    void signalBadSyscall(int code, pid_t pid, uid_t uid);
    void signalTerminate(int code, pid_t pid, uid_t uid);
    void signalBreakpoint(void *addr, int code, pid_t pid, uid_t uid);
    void signalTerminalInput(int code, pid_t pid, uid_t uid);
    void signalTerminalOutput(int code, pid_t pid, uid_t uid);
    void signalUrgent(int code, pid_t pid, uid_t uid);
    void signalUser1(int code, pid_t pid, uid_t uid);
    void signalUser2(int code, pid_t pid, uid_t uid);
    void signalVirtualAlarm(int code, pid_t pid, uid_t uid);
    void signalCpuTimeLimitExceeded(int code, pid_t pid, uid_t uid);
    void signalFileSizeLimitExceeded(int code, pid_t pid, uid_t uid);
    void signalWindowResize(int code, pid_t pid, uid_t uid);

public slots:

protected:
    void connectNotify(const QMetaMethod &signal) override;
    void disconnectNotify(const QMetaMethod &signal) override;

private slots:
    void onSocketData();

private:
    struct SignalState {
        int refCount;
    };
    std::array<SignalState, _NSIG> _signalInfo;
    std::array<int,2> _socket;
    QSocketNotifier *_notifier;

    UnixSignalHandler();

    static int signalFromMethod(const QMetaMethod &signal);
    void doAttachSignal(int signal);
    void doDetachSignal(int signal);

    static void signalHandler(int signal, siginfo_t *info, void *ucontext);
};

#endif // UTIL_UNIXSIGNALHANDLER_H
