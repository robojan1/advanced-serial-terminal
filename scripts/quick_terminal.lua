--------------------------------------------------
-- Name: Quick Terminal
-- Author: Robbert-Jan de Jager
-- Date: 2019-09-01
-- Description: This will open the default terminal 
--      emulator and display it for the user.
--------------------------------------------------


-- Create the virtual terminal emulator window
win = vte.new()

-- Create the terminal emulator
sh = shell.new()

-- The Function handler that is called when the user enters keys in the VTE
function vteCallback(data)
    sh:write(data)
end
win:registerInputCallback(vteCallback)

-- The Function handler that is called when the VTE is resized
function vteResized(width, height)
    print(string.format('resized: %dx%d', width, height))
    sh:resize(width, height)
end
win:registerResizeCallback(vteResized)

-- The Function handler that is called when the terminal application wants to display some data
function shellDataCallback(data)
    win:write(data)
end
sh:registerDataCallback(shellDataCallback)

-- The function handler that is called when the terminal application exits
function shellExitCallback(normal, status)
    print(string.format('Shell exited %s with status %d', normal and 'normally' or 'abnormally', status))
    win:close()
end
sh:registerExitCallback(shellExitCallback)

-- Display the window
add_window(win)
