--------------------------------------------------
-- Name: Quick Serial
-- Author: Robbert-Jan de Jager
-- Date: 2019-09-14
-- Description: This will open the default a serial
--      connection and display it for the user.
--------------------------------------------------

-- Open the serial settings dialog
accepted, parameters = dialogs.showSerialSettings()
if not accepted then
    print("Opening canceled")
    return
end

-- Create the serial port from the settings
port = serial.new(parameters)

-- Create the virtual terminal emulator window
win = vte.new()

-- The Function handler that is called when the user enters keys in the VTE
function vteCallback(data)
    port:write(data)
end
win:registerInputCallback(vteCallback)

-- The Function handler that is called when the terminal application wants to display some data
function portDataCallback(data)
    win:write(data)
end
port:registerDataCallback(portDataCallback)

-- Display the window
add_window(win)

